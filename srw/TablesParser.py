# -*- coding: utf-8 -*-

'''
Class for abstracting pytables objects
'''

import tables

class TablesParser(object):
    '''
    Base class for the parser
    '''
    def __init__(self, filename):
        '''
        Constructor

        Takes the filename of the pytables object
        '''
        self.filename = filename
        self.tab = tables.openFile(self.filename, 'r')

    def close(self):
        '''
        Close the database object
        '''
        self.tab.close()

    def getNode(self, path, nodename=None):
        '''
        Returns a node of the database
        '''
        if nodename:
            return self.tab.getNode(path, nodename)
        else:
            return self.tab.getNode(path)

    def getTable(self, path, tablename):
        '''
        Returns a table object

        If the requested node is not a table then raise
        an exception
        '''
        node = self.getNode(path, tablename)

        if type(node) == tables.table.Table:
            return node
        else:
            raise RuntimeError("Requested node is not a table")

