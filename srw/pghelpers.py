'''
Some helper functions for using ppgplot.

Most are context managers to be used like

>>> with open_plot:
...     # Plot stuff
>>>
>>> # Plot is closed here

The above example will open a plotting device and automatically
close it when the block exits.
'''

import numpy as np
import contextlib
import ppgplot as pg


@contextlib.contextmanager
def open_plot(device="1/xs", buf=True, iden=False):
    '''
    Open a plot device and automatically close it
    '''
    pg.pgopen(device)
    if buf:
        pg.pgbbuf()
    yield
    if buf:
        pg.pgebuf()

    if iden:
        pg.pgiden()

    pg.pgclos()


@contextlib.contextmanager
def change_colour(new_colour):
    '''
    Change the plot colour and change it back automatically
    '''
    # Set up colour map if the argument is a string
    colours = {'white': 0,
            'black': 1,
            'red': 2,
            'green': 3,
            'blue': 4,
            'cyan': 5,
            'magenta': 6,
            'yellow': 7,
            'orange': 8,
            'lime': 9,
            'w': 0,
            'k': 1,
            'r': 2,
            'g': 3,
            'b': 4,
            'c': 5,
            'm': 6,
            'y': 7,
            'o': 8,
            'l': 9,
            }

    old_colour = pg.pgqci()
    if isinstance(new_colour, str):
        try:
            pg.pgsci(colours[new_colour.lower()])
        except KeyError:
            raise RuntimeError('Invalid colour, choices are: [{:s}]'.format(
                ', '.join(['\'{}\''.format(k) for k in colours.keys()])))
    else:
        pg.pgsci(int(new_colour))
    yield
    pg.pgsci(old_colour)


@contextlib.contextmanager
def change_linestyle(new_linestyle):
    '''
    Change the line style and change it back automatically
    '''
    old_linestyle = pg.pgqls()
    pg.pgsls(new_linestyle)
    yield
    pg.pgsls(old_linestyle)


@contextlib.contextmanager
def change_fillstyle(new_fillstyle):
    '''
    Change the fill style and change it back automatically
    '''
    old_fillstyle = pg.pgqfs()
    pg.pgsfs(new_fillstyle)
    yield
    pg.pgsfs(old_fillstyle)


@contextlib.contextmanager
def change_characterheight(new_ch):
    old_ch = pg.pgqch()
    pg.pgsch(new_ch)
    yield
    pg.pgsch(old_ch)


@contextlib.contextmanager
def change_linewidth(new_lw):
    old_lw = pg.pgqlw()
    pg.pgslw(new_lw)
    yield
    pg.pgslw(old_lw)


def autorange(xdata, ydata, xmin=None, xmax=None, ymin=None, ymax=None,
        setup=True, logx=False, logy=False, edge=0.1):
    '''
    Sets up the plotting environment giving a nice edge around the plotting
    environment. If setup is true then pgenv is called.

    Arguments x/y min/max allow for specific values to set the range to

    The x/y ranges are returned in two tuples
    '''

    if logx:
        x_range = (
                min(xdata) - edge if not xmin else xmin,
                max(xdata) + edge if not xmax else xmax,
                )
    else:
        x_range = (
                min(xdata) * (1. - edge) if not xmin else xmin,
                max(xdata) * (1. + edge) if not xmax else xmax,
                )

    if logy:
        y_range = (
                min(ydata) - edge if not ymin else ymin,
                max(ydata) + edge if not ymax else ymax,
                )
    else:
        y_range = (
                min(ydata) * (1. - edge) if not ymin else ymin,
                max(ydata) * (1. + edge) if not ymax else ymax,
                )

    log_index = 0
    if logx:
        log_index += 10

    if logy:
        log_index += 20

    if setup:
        pg.pgenv(x_range[0], x_range[1], y_range[0], y_range[1], 0, log_index)

    return (x_range, y_range)


def set_colourmap(cmap, N=20, alpha=False):
    '''
    Uses a matplotlib cmap to set the colour table
    '''
    L = np.linspace(0, 1, N)
    R, G, B, A = zip(*cmap(L))

    R, G, B = [np.array(a) for a  in [R, G, B]]
    pg.pgctab(L, R, G, B)


def create_translation_matrix(data, x_range, y_range):
    '''
    Creates the translation matrix for mapping the data stored in data to the
    world coordinate range of x_range[0] -> x_range[1] and y_range[0] ->
    y_range[1].

    This is taken directly from the ppgplot code, but with some corrections
    that I believe should be made.

    A unity translation matrix is:

        0, 1, 0,
        0, 0, 1

    The first term is the constant offset and the final make up the translation
    matrix itself.
    '''

    # Put in the same variables as the source code
    r1 = 0
    r2 = data.shape[0]
    c1 = 0
    c2 = data.shape[1]
    x1, x2 = x_range
    y1, y2 = y_range

    t1 = (x2 - x1) / (c2 - c1)
    t0 = x1 - (t1 * c1) / 2.
    t2 = 0.

    t5 = (y2 - y1) / (r2 - r1)
    t4 = 0.
    t3 = y1 - (t5 * r1) / 2.

    return np.array([t0, t1, t2, t3, t4, t5])
