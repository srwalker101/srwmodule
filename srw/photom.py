"""
Starlink autophotom simplifier.
Simon Walker
--------------------------------------------------

Description

Autophotom from Starlink provides a simple method for performing
aperture photometry on a fits file given a pair of x/y coordinates and
an aperture radius. Centroiding can be performed and the software
calculates sky background and error parameters.

Using this software in a non-interactive way is difficult despite the
fact that it is an "automatic" version of Starlink's own Photom package.
The software is incompatible with fits files and certain commands must
be run before photometry can be performed.

The main commands have a verbose parameter which allows for checking the
output is as expected, but for general use it is quite lengthy.

Usage

0. Ensure Starlink is in your path
The script assumes the required commands are in your PATH variable. It
does this by looking to see if the variable

STARLINK_DIR

is set. It does not matter whether you are using Bash or Csh for this,
but it must be set before running any commands. At Warwick (as of at
least 22/11/11), either your shell source file must have a line:

    csh: source /home/astro/phsaap/.starlinkrc
    bash: source /home/astro/phsaap/.starlinkbrc

or if you know what you're doing you can set the variables up manually
but ensure that $STARLINK_DIR is set.


1. Convert your file

*** Update

THIS STEP IS NOT REQUIRED.

The constructor of the autophotomCoords object automatically converts
the file if needed. All temporary files are stored in the users
/tmp directory and so do not clutter up the
filesystem.

*** Update

First the fits data file must be converted to a Starlink sdf file. If
your data is already in this format then this step can be skipped.

Set up the converter object. The dir argument can specify placing the
source file in a directory other than the current one (default).

>>> converter = srw.photom.Converter(filename, dir=dir)

Then call the convert command. This will return the converted filename.
The verbose command prevents printing to screen.

>>> NewFilename = converter.convert(verbose=False)

2. Perform photometry
The way Autophotom runs is to take a source data file in as a command
line argument which is a placeholder for the final output argument. The
creation of an autophotomCoords object will create this file.

>>> photometer = autophotomCoords(x=x, y=y, srcfile=NewFilename,
        centro=True, apsize=4.)

The arguments should be fairly self explanatory. This sets up the
photometry object and sets internal variables.

Two settings are meant to be instrument specific: the gain and exposure
time. These are by default 1 (for both) and can be adjusted by setting
the parameter in the params member variable of the autophotomCoords
object:

>>> photometer.params['padu'] = 10.
>>> photometer.params['exptime'] = 30.

The rest of the settings can be left at default for most cases since they
handle the workings of the centroiding and error calculations, but if you wish
to change them then the above method is used.

Photometry is finally performed by the photometry() method.

>>> results = photometer.photometry(verbose=False)

This will return a dictionary of the results. You can explore the
dictionary at your leisure, but the returned keys are:


    * index
    * scalein
    * signal
    * sky
    * flux
    * radius
    * y
    * x
    * meanflux
    * scaleout
    * meanerr

See the Autophotom documentation for an explanation of these keys.

Settings

Autophotom does have some settings which enable eg. how to estimate the
background which I have set at reasonable defaults. Should you wish
these changing either contact me (simon.walker@warwick.ac.uk) and I
shall put in some methods to change the parameters, or create a class
which inherits from this and change the autophotomCoords.params
dictionary. The defaults are listed below and any skipped ones will
revert to Autophotom's defaults. You should not need to change anything
else since inheriting the class will inherit the methods, such are the
joys of OOP!

Program defaults:

biasle: 0
etime: 1
exsource: constant
fixann: false
usemags: false
maxiter: 9
maxshift: 20
optima: false
padu: 1.2
photon: 1
positive: true
sature: 1.7E30
search: 12
skyest: 2
toler: 0.05
usemask: false

"""

import sys
import os
from subprocess import Popen, STDOUT, PIPE
from tempfile import gettempdir
import os.path
import warnings


def check_for_starlink():
    """Function checks for the existance of $STARLINK_DIR/etc/profile
    and if it doesn't exist, raises an OSError"""
    starlink_dir = os.environ["STARLINK_DIR"]
    profile_file = os.path.join(starlink_dir, 'etc', 'profile')
    if not os.path.isfile(profile_file):
        raise OSError("Cannot find starlink software, ensure "\
                "$STARLINK_DIR is set correctly")
    return profile_file

class Coord(object):
    def __init__(self, x=0., y=0.):
        super(Coord, self).__init__()
        self.x = x
        self.y = y

    def plot(self, ax, colour, **kwargs):
        ax.plot(self.x, self.y, marker='x', color=colour,
                label=kwargs['label'])

    def fromList(self, l):
        self.x = l[0]
        self.y = l[1]

    def __str__(self):
        return "(%.2f, %.2f)" % (self.x, self.y)

    def __add__(self, o):
        """
        Addition function for other object type Coord

        >>> c = Coord(2., 4.)
        >>> c += Coord(1., 1.)
        >>> print c
        (3.00, 5.00)

        """
        c = Coord(self.x, self.y)
        c.x += o.x
        c.y += o.y
        return c

    def __rsub__(self, o):
        c = Coord(o.x, o.y)
        c.x -= self.x
        c.y -= self.y
        return c

    def __sub__(self, o):
        c = Coord(self.x, self.y)
        c.x -= o.x
        c.y -= o.y
        return c

    def gradient(self):
        try:
            return self.y / self.x
        except ZeroDivisionError:
            print >> sys.stderr, "Error: points are directly above/below "\
                    "each other causing divide by zero"
            sys.exit(1)

    def plot(self, ax, type='rx', zorder=1, alpha=1.):
        ax.plot([self.x,], [self.y,], type, zorder=zorder, alpha=alpha)

def distance(coord1, coord2):
    return sqrt((coord2.x - coord1.x)**2 + (coord2.y - coord1.y)**2)

class Converter(object):
    """
    Class controls converting a fits file from fits format to
    starlink format
    """
    def __init__(self, filename, dir=None):
        """
        Call with the filename argument, and an optional directory
        argument in which the file will be placed
        """
        super(Converter, self).__init__()
        self.filename = filename
        self.filename = os.path.abspath(self.filename)
        stub = os.path.basename(self.filename)
        if dir:
            loc = dir
        else:
            loc = gettempdir()

        self.sdfname = os.path.join(
                loc,
                os.path.splitext(stub)[0] + ".sdf",
                )

    def convert(self, verbose=False):
        """
        Convert the file to autophotom format and return
        the sdf name
        """
        profile_file = check_for_starlink()
        cmd = "source %s && convert && fits2ndf in='%s' out='%s'" % (
                profile_file, self.filename, self.sdfname,
                )
        if verbose:
            p = Popen(cmd, shell=True)
        else:
            p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        p.communicate()

        return self.sdfname

class autophotomCoords(object):
    """
    Class controls handling the photometry of a single file after it has
    been converted to starlink format

    """
    params = {
"biasle": "0",
"etime": "1",
"exsource": "constant",
"fixann": "false",
"usemags": "false",
"maxiter": "9",
"maxshift": "20",
"optima": "false",
"padu": "1.0",
"photon": "1",
"positive": "true",
"sature": "1.7E30",
"search": "12",
"skyest": "2",
"toler": "0.05",
"usemask": "false",
}
    def __init__(self, x, y, srcfile, centro, apsize=4., dir=None, remove=True):
        super(autophotomCoords, self).__init__()
        self.x = x
        self.y = y
        self.dir = dir
        self.remove = remove

        if self.dir:
            self.temppath = self.dir
        else:
            self.temppath = gettempdir()

        self.outfile = os.path.join(self.temppath, "output_photom.dat")
        self.coordsfile = os.path.join(self.temppath, 'coords.dat')
        self.converted = False

        if self.x < 0 or self.y < 0:
            warnings.warn("Requested point is not on the image")

        self.srcfile = srcfile

        if not os.path.isfile(self.srcfile):
            raise IOError("Cannot find source file: '%s'" % (self.srcfile,))

        # Ensure the file is in the correct format
        self.convertFile()

        self.apsize = apsize
        if self.apsize <= 0:
            raise ValueError("Aperture size cannot be <= 0")

        self.create_initial_file()


        self.params['centro'] = centro

    def convertFile(self):
        FitsExtensions = ['.FITS', ".FIT"]
        ValidExtensions = FitsExtensions + [".SDF",]

        FileExtension = os.path.splitext(self.srcfile)[-1].upper()
        if FileExtension not in ValidExtensions:
            raise OSError("File type not valid. Requires a .sdf or .fits/.fit file")

        if FileExtension in FitsExtensions:
            # fits file found, must convert
            converter = Converter(self.srcfile, dir=self.dir)
            self.srcfile = converter.convert()

            self.converted = True







    def source_line(self):
        line1 = "1 %f %f 0.0 0.0 0.0 0.0 ? %f 0.0 0.0 annulus circle" % (
                self.x, self.y, self.apsize)
        line2 = "#ANN 1 1.77 3.79"
        return "\n".join([line1, line2])

    def create_initial_file(self):
        f = open(self.coordsfile, 'w')
        f.write(self.source_line())
        f.close()

    def photometry(self, verbose=False):
        """
        Perform aperture photometry on the file. Return a dictionary of the
        results.
        """
        profile_file = check_for_starlink()
        cmd = "source %s && photom && autophotom" % (profile_file,)
        cmd += ' in=%s infile=%s outfile=%s' % (self.srcfile,
                self.coordsfile, self.outfile)

        for n, v in self.params.iteritems():
            cmd += ' ' + '='.join((str(n), str(v)))

        if verbose:
            p = Popen(cmd, shell=True)
        else:
            p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        p.communicate()

        # Store the result in a dictionary
        result = {}

        with open(self.outfile, "r") as f:
            lines = f.readlines()

        # Parse the first line
        parts = lines[0].split()
        result['index'] = int(parts[0])
        result['x'] = float(parts[1])
        result['y'] = float(parts[2])
        try:
            result['meanflux'] = float(parts[3])
        except ValueError:
            '''
            The flux value is unreadable, due to probably
            a saturated star
            '''
            result['meanflux'] = 0.
            warnings.warn("Invalid mean flux value measured. "
                    "Probably due to a saturated star")

        result['meanerr'] = float(parts[4])
        result['sky'] = float(parts[5])
        result['flux'] = float(parts[6])
        result['signal'] = parts[7]
        result['radius'] = float(parts[8])

        # and the second line
        parts = lines[1].split()
        result['scalein'] = float(parts[2])
        result['scaleout'] = float(parts[3])

        # Throw some warnings
        if result['signal'] != "OK":
            warnings.warn("Signal code is not OK")

        return result

