'''
This file contains the constants required
by my WASP selection effects tools
'''


rJup = 71492E3
mJup = 1.8986E27
rSun = 6.995E8
AU = 1.496E11
mSun = 1.9891E30
secondsInMinute = 60.
secondsInHour = 60. * secondsInMinute
secondsInDay = 24. * secondsInHour
radiansInDegree = 2. * 3.14 / 360.
degreesInRadian = 360. / 2. / 3.14
GravConst = 6.67E-11
tSun = 5778.

