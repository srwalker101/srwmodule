#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module contains the code required to get the field names
and information from a WASP data file.
"""

class WASPFieldParser(object):
    """
    Class to handle parsing a WASP data file and parsing the
    number of fields and their names and information from
    the file.
    """
    def __init__(self, headerobj):
        """
        Constructor, takes a pyfits.header object
        as the initial argument
        """
        self.header = headerobj

        self.nfields = self.header['nfields']

        # load all the fields into a field list
        self.fields = []
        for i in range(self.nfields):
            # create the field name from the integer
            fieldname = "FIELD_%02d" % (i + 1)

            # get the string containing field information
            fieldstring = self.header[fieldname]

            # create a Field object
            fieldobj = Field(fieldstring)

            # append the field object to the master list
            self.fields.append(fieldobj)

    def __getitem__(self, y):
        """
        For accessing elements with a list style syntax
        """
        return self.fields[y]

    def __getslice(self, i, j):
        """
        For accessing elements with a slice style syntax
        """
        return self.fields[i:j]

    def __str__(self):
        """
        Prints the field list object
        """
        return self.fields.__str__()

    def __eq__(self, other):
        """
        Test for equality
        """
        # simple first test: if the length of the field list is different
        # then the objects are different
        if len(self.fields) != len(other.fields):
            return False
        else:
            same = True
            for i in range(len(self.fields)):
                #print self.fields[i], other.fields[i]
                same = same and (self.fields[i] == other.fields[i])


            return same







class Field(object):
    """
    Object which contains all of the field arguments.
    ---

    Contains:
        * cameraid  - camera id
        * startdate - field start date
        * ra, dec   - field center coordinates (to 1 arcminute precision)
    """
    cameraid = None
    startdate = None
    ra = None
    dec = None
    def __init__(self, fieldstring):
        import datetime
        """
        Initialiser, takes a string argument
        """
        self.fieldstring = fieldstring

        # split up the string into subsections with _
        sections = self.fieldstring.split("_")

        fieldvalues = {
                "coords": sections[0],
                "cameraid": sections[1],
                "date": sections[2]
                }

        # camera id is already set
        self.cameraid = int(fieldvalues['cameraid'])

        # get the ra and dec (to 1minute precision),
        # first two characters are SW
        rastr = fieldvalues['coords'][2:6]
        sign = fieldvalues['coords'][6]
        decstr = fieldvalues['coords'][7:]

        # convert rastr to degrees
        self.ra = raToDegrees(rastr)
        self.dec = decToDegrees(decstr)

        if sign == '-':
            self.dec *= -1.

        # get the date as a datetime.date object
        splitdate = fieldvalues['date'].split("-")
        year = int(splitdate[0])
        month = int(splitdate[1])
        day = int(splitdate[2])
        self.startdate = datetime.date(year, month, day)





    def __str__(self):
        """
        For printing the field object.

        Prints a nice string with all the important information.
        """
        #return self.fieldstring
        string = "Center: (%f, %f)" % (self.ra, self.dec)
        string += ", date: "
        string += str(self.startdate)
        string += ", camera: "
        string += str(self.cameraid)
        return string

    def __repr__(self):
        """
        Function for when a container holds this object
        Returns the same as __str__().
        """
        return self.__str__()

    def __eq__(self, other):
        """
        For checking equality
        """
        return (self.startdate == other.startdate) and (self.ra == other.ra) and \
                (self.dec == other.dec) and (self.cameraid == other.cameraid)


def raToDegrees(rastr):
    """
    Internal function mostly, converts a hhmm string to degrees.
    """
    hour = rastr[:2]
    minute = rastr[2:]
    return 15. * (float(hour) + float(minute) / 60.)

def decToDegrees(decstr):
    """
    Internal function mostly, converts a ddmm string to degrees
    """
    degrees = decstr[:2]
    minute = decstr[2:]
    return float(degrees) + float(minute) / 60.

