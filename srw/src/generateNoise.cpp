#include "generateNoise.h"
#include <boost/python.hpp>
#include <vector>


#include <nr/nr3.h>
#include <nr/ran.h>
#include <nr/gamma.h>
#include <nr/deviates.h>
#include <fftw3.h>

NoiseGen::NoiseGen(int seed)
{
    this->seed = seed;
	this->gen = new Normaldev_BM(0., 1., this->seed); /* generator for gaussian deviates */
}

NoiseGen::~NoiseGen()
{
    delete this->gen;
}

boost::python::list NoiseGen::vector(double beta, int length)
{

    fftw_complex *in = (fftw_complex*)fftw_malloc(length*sizeof(fftw_complex));
    fftw_complex *fftout = (fftw_complex*)fftw_malloc(length*sizeof(fftw_complex));


    int i, count;
    for (count = length-1, i=1; i<length/2; i++, count--)
    {
        in[i][0] = gen->dev() * sqrt(0.5*pow(1./(double)i, beta));
        in[i][1] = gen->dev() * sqrt(0.5*pow(1./(double)i, beta));

        in[count][0] = in[i][0];
        in[count][1] = -in[i][1];



    }


    fftw_plan p = fftw_plan_dft_1d(length, in, fftout, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);

    boost::python::list out;

    for (int i=0; i<length; i++)
    {
        //out[i] = fftout[i][0];
        out.append(fftout[i][0]);
    }

    /*  normalise to av/sd */
    //double avVal = 0.;
    //double sdVal = 0.;
    //avstd_list(out, length, sdVal, avVal);
    
    
    //cout << "Raw: " << avVal << " " << sdVal << endl;
    //cout << "Target: " << targetAverage << " " << targetSd << endl;

    //for (int i=0; i<length; i++)
    //{
        //out[i] -= avVal;
        //out[i] *= targetSd / sdVal;
        //out[i] = out[i] + targetAverage - avVal;
    //}

    //avstd(out, length, sdVal, avVal);
    //cout << "Fixed: " << avVal << " " << sdVal << endl;

    fftw_free(in);
    fftw_free(fftout);
    fftw_destroy_plan(p);


    return out;

}

#define SQ(x) ((x)*(x))

void avstd_list(boost::python::list &arr, int len, double &sd, double &av)
{
    /* extract the C data */
    //vector<double> arr_c = boost::python::extract<vector<double> >(arr);
    double *arr_c = new double[len];
    for (int i=0; i<len; i++)
    {
        arr_c[i] = boost::python::extract<double>(arr[i]);
    }

    sd = 0.;
    av = 0.;
    for (int i=0; i<len; i++)
    {
        av += arr_c[i];
    }
    av /= (double)len;
    for (int i=0; i<len; i++)
    {
        sd += (arr_c[i] - av) * (arr_c[i] - av);
    }
    sd /= (double)(len-1);
    sd = sqrt(sd);
    delete[] arr_c;
}

void avstd(double *arr, int len, double &sd, double &av)
{
    sd = 0.;
    av = 0.;
    for (int i=0; i<len; i++)
    {
        av += arr[i];
    }
    av /= (double)len;
    for (int i=0; i<len; i++)
    {
        sd += (arr[i] - av) * (arr[i] - av);
    }
    sd /= (double)(len-1);
    sd = sqrt(sd);
}


#undef SQ

using namespace boost::python;
BOOST_PYTHON_MODULE(Noise)
{
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
    class_<NoiseGen>("NoiseGen", init<int>())
        //.def("setSeed", &NoiseGen::setSeed)
        //.def("getSeed", &NoiseGen::getSeed)
        .def("vector", &NoiseGen::vector)
        .def_readwrite("seed", &NoiseGen::seed)
        ;
}
