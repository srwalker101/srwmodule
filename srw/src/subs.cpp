#include <Python.h>
#include <stdio.h>
#include <numpy/arrayobject.h>
#include <fftw3.h>
#include <nr/nr3.h>
#include <nr/ran.h>
#include <nr/gamma.h>
#include <nr/deviates.h>

static PyObject *bin_wrap(PyObject *self, PyObject *args, PyObject *keywds)/*{{{*/
{
	PyObject *data = NULL;
	int nbins;
	int errFlag = 1;

	static char *kwlist[] = {"data", "nbins", "errors", NULL};

	/*if (!PyArg_ParseTuple(args, "Oi", &data, &nbins)) return NULL;*/
	if (!PyArg_ParseTupleAndKeywords(args, keywds, "Oi|i:bin", kwlist,
				&data, &nbins, &errFlag)) return NULL;

	/* check input arguments */
	if (!PyArray_Check(data))
	{
		PyErr_SetString(PyExc_TypeError, "Input data is not an array");
		return NULL;
	}

	if (PyArray_NDIM(data) != 1)
	{
		PyErr_SetString(PyExc_ValueError, "Input array is not one dimensional");
		return NULL;
	}





	npy_intp n = PyArray_Size(data);

	if (nbins < 1 || nbins > n)
	{
		char buf[80];
		sprintf(buf, "Binning value must be in the range 0 < %d", (int)n);
		PyErr_SetString(PyExc_ValueError, buf);
		return NULL;
	}

	PyObject *xobj = PyArray_FROM_OTF(data, NPY_DOUBLE, NPY_IN_ARRAY);
	double *p1 = (double*)PyArray_DATA(xobj);
	int fullsize = n;
	while (fullsize % nbins)
	{
		--fullsize;
	}
	int newsize = fullsize / nbins;
	double *binout = new double[newsize];
	double *binerr = new double[newsize];

	int i, j;
	int count = 0;
	for (i=0; i<fullsize; i+=nbins)
	{
		double av = 0;
		double err = 0;
		for (j=0; j<nbins; j++)
		{
			av += p1[i+j];
			/*err += p1[i+j] * p1[i+j];*/
		}
		av /= (double)nbins;

		binout[count] = av;

		for (j=0; j<nbins; j++)
		{
			err += (p1[i+j] - av) * (p1[i+j] - av);
		}
		err /= (double)nbins;

		binerr[count] = sqrt(err);

		++count;
	}



	npy_intp outsize = newsize;

	/*PyArrayObject *outarr = (PyArrayObject*)*/
	/*PyArray_SimpleNewFromData(1, &val, PyArray_DOUBLE, result);*/

	PyArrayObject *outav = (PyArrayObject*)
		PyArray_SimpleNewFromData(1, &outsize, PyArray_DOUBLE, binout);
	PyArrayObject *outerr = (PyArrayObject*)
		PyArray_SimpleNewFromData(1, &outsize, PyArray_DOUBLE, binerr);


	/*return Py_BuildValue("O", outarr);*/
	/*return Py_BuildValue("ii", fullsize, newsize);*/
	if (errFlag)
	{
		return Py_BuildValue("OO", outav, outerr);
	}
	else
	{
		return Py_BuildValue("O", outav);
	}
}/*}}}*/

static PyObject *bin_sum(PyObject *self, PyObject *args, PyObject *keywds)/*{{{*/
{
	PyObject *data = NULL;
	int nbins;
	int errFlag = 1;

	static char *kwlist[] = {"data", "nbins", "errors", NULL};

	/*if (!PyArg_ParseTuple(args, "Oi", &data, &nbins)) return NULL;*/
	if (!PyArg_ParseTupleAndKeywords(args, keywds, "Oi|i:bin", kwlist,
				&data, &nbins, &errFlag)) return NULL;

	/* check input arguments */
	if (!PyArray_Check(data))
	{
		PyErr_SetString(PyExc_TypeError, "Input data is not an array");
		return NULL;
	}

	if (PyArray_NDIM(data) != 1)
	{
		PyErr_SetString(PyExc_ValueError, "Input array is not one dimensional");
		return NULL;
	}





	npy_intp n = PyArray_Size(data);

	if (nbins < 1 || nbins > n)
	{
		char buf[80];
		sprintf(buf, "Binning value must be in the range 0 < %d", (int)n);
		PyErr_SetString(PyExc_ValueError, buf);
		return NULL;
	}

	PyObject *xobj = PyArray_FROM_OTF(data, NPY_DOUBLE, NPY_IN_ARRAY);
	double *p1 = (double*)PyArray_DATA(xobj);
	int fullsize = n;
	while (fullsize % nbins)
	{
		--fullsize;
	}
	int newsize = fullsize / nbins;
	double *binout = new double[newsize];
	double *binerr = new double[newsize];

	int i, j;
	int count = 0;
	for (i=0; i<fullsize; i+=nbins)
	{
		double sum = 0;
		double err = 0;
        double av = 0;
		for (j=0; j<nbins; j++)
		{
			sum += p1[i+j];
			/*err += p1[i+j] * p1[i+j];*/
            av += sum;
		}
		//sum /= (double)nbins;
        av /= (double)nbins;

		binout[count] = sum;

		for (j=0; j<nbins; j++)
		{
			err += (p1[i+j] - av) * (p1[i+j] - av);
		}
		err /= (double)nbins;

		binerr[count] = sqrt(err);

		++count;
	}



	npy_intp outsize = newsize;

	/*PyArrayObject *outarr = (PyArrayObject*)*/
	/*PyArray_SimpleNewFromData(1, &val, PyArray_DOUBLE, result);*/

	PyArrayObject *outav = (PyArrayObject*)
		PyArray_SimpleNewFromData(1, &outsize, PyArray_DOUBLE, binout);
	PyArrayObject *outerr = (PyArrayObject*)
		PyArray_SimpleNewFromData(1, &outsize, PyArray_DOUBLE, binerr);


	/*return Py_BuildValue("O", outarr);*/
	/*return Py_BuildValue("ii", fullsize, newsize);*/
	if (errFlag)
	{
		return Py_BuildValue("OO", outav, outerr);
	}
	else
	{
		return Py_BuildValue("O", outav);
	}
}/*}}}*/

#define SQ(x) ((x)*(x))/*{{{*/

void avstd(double *arr, int len, double &sd, double &av)
{
    sd = 0.;
    av = 0.;
    for (int i=0; i<len; i++)
    {
        av += arr[i];
    }
    av /= (double)len;
    for (int i=0; i<len; i++)
    {
        sd += (arr[i] - av) * (arr[i] - av);
    }
    sd /= (double)(len-1);
    sd = sqrt(sd);
}


#undef SQ/*}}}*/

//static PyObject *noiseGen(PyObject *self, PyObject *args, PyObject *keywds)
//{
    //double beta;
    //int length;
    //double targetAverage;
    //double targetSd;
    //int seed;


    //static char *kwlist[] = {"beta", "length", "targetAverage", "targetSd", "seed", NULL};

    //if (!PyArg_ParseTupleAndKeywords(args, keywds, "diddi", kwlist,
                //&beta, &length, &targetAverage, &targetSd, &seed)) return NULL;

    //Normaldev_BM gen(0., 1., seed);

    //fftw_complex *in = (fftw_complex*)fftw_malloc(length*sizeof(fftw_complex));
    //fftw_complex *fftout = (fftw_complex*)fftw_malloc(length*sizeof(fftw_complex));


    //int i, count;
    //for (count = length-1, i=1; i<length/2; i++, count--)
    //{
        //in[i][0] = gen.dev() * sqrt(0.5*pow(1./(double)i, beta));
        //in[i][1] = gen.dev() * sqrt(0.5*pow(1./(double)i, beta));

        //in[count][0] = in[i][0];
        //in[count][1] = -in[i][1];



    //}


    //fftw_plan p = fftw_plan_dft_1d(length, in, fftout, FFTW_BACKWARD, FFTW_ESTIMATE);
    //fftw_execute(p);

    //double *out = new double[length];

    //for (int i=0; i<length; i++)
    //{
        //out[i] = fftout[i][0];
    //}

    //[>  normalise to av/sd <]
    //double avVal = 0.;
    //double sdVal = 0.;
    //avstd(out, length, sdVal, avVal);

    //for (int i=0; i<length; i++)
    //{
        //out[i] -= avVal;
        //out[i] *= targetSd / sdVal;
        //out[i] += targetAverage - avVal;
    //}

    //fftw_free(in);
    //fftw_free(fftout);
    //fftw_destroy_plan(p);

    //npy_intp outlength = length;

    //PyArrayObject *outarr = (PyArrayObject*)
        //PyArray_SimpleNewFromData(1, &outlength, PyArray_DOUBLE, out);


    //return Py_BuildValue("O", outarr);
//}


static PyMethodDef methods[] = {
	{"bin", (PyCFunction)bin_wrap, METH_VARARGS | METH_KEYWORDS,
		"(b, e) = bin(data, binNumber, errors=True)\n\n"
			"-- data and the number of values to bin by\n"
			"   is input and the binned data is returned.\n"
			"   Optionally the errors are returned also (by default)\n"},
	{"bin_sum", (PyCFunction)bin_sum, METH_VARARGS | METH_KEYWORDS,
		"(b, e) = bin(data, binNumber, errors=True)\n\n"
            "Similar to 'bin' function but returns sums instead of averages\n"
			"-- data and the number of values to bin by\n"
			"   is input and the binned data is returned.\n"
			"   Optionally the errors are returned also (by default)\n"},
    //{"noise", (PyCFunction)noiseGen, METH_VARARGS | METH_KEYWORDS,
        //"lc = noise(beta, length, target_average, target_sd, seed)\n\n"
            //"-- Generate coloured noise with parameters:\n"
            //"   * beta = colour of the noise\n"
            //"   * length = length of lightcurve\n"
            //"   * target_average = target average of the lightcurve\n"
            //"   * target_sd = target scatter of the lightcurve\n"
            //"   * seed = seed for random number generator\n"    
    //},









	{NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initsubs(void)
{
	Py_InitModule("subs", methods);
	import_array();
}

