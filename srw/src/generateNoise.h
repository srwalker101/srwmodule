#ifndef SECONDMETHOD_H

#define SECONDMETHOD_H

#include <boost/python/list.hpp>
#include <boost/python/numeric.hpp>


class Normaldev_BM;

class NoiseGen
{
    public:
        NoiseGen(int seed);

		/* 	seed accessor functions */
		//void setSeed(int seed) { this->seed = seed; }
		//int getSeed() { return this->seed; }

		boost::python::list vector(double beta, int length);

		~NoiseGen();


		int seed;

	private:
		Normaldev_BM *gen;
};

void generateColouredNoise(double beta, int length, double targetAverage, double targetSd, double *out);

void avstd_list(boost::python::list &arr, int len, double &sd, double &av);
void avstd(double *arr, int len, double &sd, double &av);

#endif /* end of include guard: SECONDMETHOD_H */
