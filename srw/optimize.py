'''
Module: function_fitter
Author: Simon Walker

For fitting a function, including a Cash minimizer
'''


import numpy as np
from scipy.optimize import minimize

class FunctionFitter(object):
    '''
    Function fitter class.
    '''
    methods = ['chisq', 'cash']

    def __init__(self, x, y, e, method='chisq'):
        '''
        Constructor to set up the variables
        '''
        self.check_valid_method(method)
        self.method = getattr(self, method)
        self.x, self.y, self.e = x, y, e
        self.fn = None
        self.callback = None
        self.results = None

    def check_valid_method(self, method):
        '''
        Checks that the method passed is valid
        '''
        assert method.lower() in self.methods, "Methods available: {}".format(self.methods)

    def cash(self, y, model, e):
        '''
        Cash minimizer (Cash, 1979)
        '''
        return 2. * np.sum(model - y * np.log(model))

    def chisq(self, y, model, e):
        '''
        "Standard" chisq minimizer
        '''
        return np.sum(((y - model) / e) ** 2)

    def set_function(self, fn):
        self.fn = fn

    def log_likelihood(self, params):
        '''
        Calculate the log likelihood
        '''
        self.check_function()
        model = self.fn(self.x, *params)
        ll = self.method(self.y, model, self.e)
        return ll

    def fit(self, p0, disp=False):
        '''
        Perform the fit
        '''
        self.check_function()
        self.results = minimize(self.log_likelihood, p0, method='nelder-mead', 
                options={'disp': disp}, callback=self.callback)

    def best_fit_params(self):
        '''
        Get the best fit parameters, after calling `fit`
        '''
        return self.results.x

    def evaluate(self, x):
        '''
        After fitting, return the model value at point x
        '''
        self.check_function()
        return self.fn(x, *self.best_fit_params())

    def check_function(self):
        '''
        Checks for a function passed
        '''
        assert self.fn, "Please specify a fitting function"

    def set_callback(self, cb):
        '''
        Set the optional callback
        '''
        self.callback = cb

