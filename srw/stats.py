# -*- coding: utf-8 -*-
import numpy as np

def mad(data, median=None, axis=None):#{{{
    ''' Calculate the median absolute deviation. This is a way to
    measure the standard deviation of a dataset whilst being unbiased
    otowards outliers.

    By convention the standard deviation is 1.4826 times the MAD. '''
    data = np.array(data)
    median = median if median else np.median(data, axis=axis)
    try:
        return np.median(np.abs(data - median), axis=axis)
    except ValueError:
        return np.median(np.abs((data.T - median).T), axis=axis)


#}}}
def rms_vs_brightness(flux, fluxerr=None, mags=False, median_method=False):#{{{
    '''
    Given a 2d array of flux and optionally fluxerr, return the 
    mean flux and frms arrays, one per object.

    By convention the x axis is time and y axis is aperture.

    If mags == True then the input data are assumed to be in 
    magnitude units.

    If median_method == True then calculate the std from the mad
    '''
    axis = 1
    if mags:
        return np.average(flux, axis=axis), np.std(flux, axis=axis)
    else:
        av_flux = (np.average(flux, axis=axis, weights=1. / fluxerr ** 2)
                if fluxerr is not None
                else np.average(flux, axis=axis))

        # Alternatively if the following condition is correct, use the median
        if median_method is None and fluxerr is None:
            av_flux = np.median(flux, axis=axis)

        std_flux = (1.4826 * mad(flux, axis=axis) if median_method
                    else np.std(flux, axis=axis))

        return av_flux, std_flux / av_flux

#}}}
def bin_simultaneously(flux, fluxerr, n):#{{{
    ''' Take a 2d array of flux and sum every n elements in a running
    average sense '''
    out = []
    for i in xrange(flux.shape[1] - n - 1):
        s = slice(i, i + n)
        selected = flux[:, s]
        selected_err = fluxerr[:, s]
        av_flux = np.average(selected, weights=1. / selected_err ** 2, axis=1)
        out.append(av_flux)

    out = np.array(out).T
    return out
#}}}
def theoretical_line(start, xend):#{{{#{{{
    ''' Create data to plot a theoretical line with a gradient of -0.5
as white noise behaves '''

    m = -0.5
    lstart = np.log10(start)
    logc = lstart[1] - m * lstart[0]
    logy = m * np.log10(xend) + logc

    return (xend, 10 ** logy)

#}}}
