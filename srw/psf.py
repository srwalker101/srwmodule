"""
This module controls the reading and writing of binary .psf files
used in the NGTS pipeline.

The Data object holds the n*3 data in the form x, y, err for the
psf.

Each PSF object has one Data object for each compass direction and
handles such things as plotting (for diagnostics) and rendering to
a file.
"""

import numpy as np
from scipy.interpolate import interp1d
import cPickle

class Data(object):
    """
    Object to hold the data information in member variables

    x, y, err
    """
    def __init__(self, filename=None, x=None, y=None, err=None):
        super(Data, self).__init__()
        if filename:
            self.loadData(filename)
        else:
            self.x = x
            self.y = y
            self.err = err

    def loadData(self, filename):
        self.x, self.y, self.err = np.loadtxt(filename, unpack=True)

    def invert(self):
        self.x = -self.x[::-1]
        self.y = self.y[::-1]
        self.err = self.err[::-1]
        return self

    def __len__(self):
        """
        Returns the number of elements in the data collection
        """
        return self.x.size

    def resize(self, newsize):
        ind = np.arange(newsize)
        d = Data()
        d.x = self.x[ind]
        d.y = self.y[ind]
        d.err = self.err[ind]
        return d

    def plot(self, ax):
        ax.errorbar(self.x, self.y, self.err)


class PSF(object):
    """
    PSF object, holds 4 Data objects for each compass direction
    """
    def __init__(self, prefix=""):
        """
        Initialise the PSF object.

        Four Data objects are created for each compass direction.
        self.ordinates is a list of each one.

        These are combined into self.combine object, with the
        length of the smallest one. Errors are combined in quadrature.
        Finally the west and south arrays are inverted
        """

        super(PSF, self).__init__()
        self.east = Data(prefix + 'east.psf')
        self.west = Data(prefix + 'west.psf')
        self.north = Data(prefix + 'north.psf')
        self.south = Data(prefix + 'south.psf')

        self.ordinates = [
                self.east,
                self.west,
                self.north,
                self.south,
                ]

        # Combine the data into one profile measurement
        minsize = min([len(x) for x in self.ordinates])


        neweast = self.east.resize(minsize)
        newwest = self.west.resize(minsize)
        newnorth = self.north.resize(minsize)
        newsouth = self.south.resize(minsize)
        newordinates = np.array([
                neweast.y,
                newsouth.y,
                newwest.y,
                newnorth.y,
                ])
        newerrs = np.array([
            neweast.err,
            newsouth.err,
            newwest.err,
            newnorth.err,
            ])


        self.combined = Data()
        self.combined.y = np.average(newordinates, weights=newerrs, axis=0)
        self.combined.x = neweast.x
        self.combined.err = np.sqrt(np.sum(newerrs**2, axis=0)) / \
                float(len(newordinates))

        # Invert the east and west data for plotting
        self.west = self.west.invert()
        self.south = self.south.invert()

        for i in np.arange(minsize)[::-1]:
            if self.combined.y[i] != 0.:
                self.maxpsfdist = self.combined.x[i]
                break

    def plot(self, ax):
        """
        Plot the psf on a matplotlib axes instance
        """
        ax.plot(self.east.x, self.east.y,
                self.west.x, self.west.y,
                self.north.x, self.north.y,
                self.south.x, self.south.y,
                )

    def render(self, filename):
        """
        Pickle the psf data to disk
        """
        outputdata = {'east': self.east,
                'west': self.west,
                'north': self.north,
                'south': self.south,
                }
        f = open(filename, 'wb')
        cPickle.dump(self, f, 1)
        f.close()

    def interpolate(self, r, kind='cubic'):
        """
        Combine psf measurements and return the interpolated
        psf at radius value r. If r is outside the measured range,
        0 is returned.
        """
        f = interp1d(self.combined.x, self.combined.y, kind=kind)
        if type(r) == np.ndarray or type(r) == list:
            outarr = np.array([])
            for val in r:
                try:
                    outarr = np.append(outarr, f(val))
                except ValueError:
                    outarr = np.append(outarr, 0.)

            return outarr
        elif type(r) == float or type(r) == int:
            try:
                returnval = f(float(r))
            except ValueError:
                returnval = 0.
            return returnval

