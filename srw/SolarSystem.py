#!/usr/bin/env python
# encoding: utf-8

'''
Defines some solar system constants
'''

EARTH_RADII = 1. / 11.209

Mercury = {
'radius':  0.3829 * EARTH_RADII,
'a': 0.466,
}

Venus = {
        'radius': 0.949 * EARTH_RADII,
        'a': 0.728,
        }

Earth = {
        'radius': EARTH_RADII,
        'a': 1.
        }

Mars = {
        'radius': 1.665 * EARTH_RADII,
        'a': 0.533,
        }

Jupiter = {
        'radius': 1.,
        'a': 5.2,
        }

Saturn = {
        'radius': 9.442 * EARTH_RADII,
        'a': 9.58,
        }

Neptune = {
        'radius': 4.007 * EARTH_RADII,
        'a': 19.229,
        }

Uranus = {
        'radius': 3.883 * EARTH_RADII,
        'a': 30.1,
        }
