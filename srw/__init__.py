"""
srw module by Simon Walker

Each submodule contains its own documentation.

CONTENTS:

    srw                 - simple functions or classes
    srw.psf             - for handling the NGTS psf information
    srw.WASPFieldParser - for parsing the WASP field names
    srw.photom          - for performing aperture photometry on a single
                            target using Autophotom
    srw.NOMADParser     - for querying the NOMAD catalogue from Python
    srw.AstErrors       - for calculating photometric errors


srw
--------

 This is a module with some subroutines by Simon Walker

 Contains a diverse bunch of routines, most of them probably useless to
 most people (including myself now!) or obselete since eg. I found numpy has
 an equivalent, or I have simply changed the way I perform the analysis.

Highlights:
    cumulative_hist     - cumulative histogram function
    ZP                  - measured zero point of the NGTS prototype, allowing for scaling
                        by exposure time
    bcolours            - quick method for printing terminal (stdout/stderr) text
                        in a specific colour
    correctForAirmass   - given a lightcurve and list of zenith
                        distances, fit the extinction coefficient and detrend
    airmassLookupTable  - find the extinction coefficient (mag per
                        airmass) at a given wavelength (interpolated
                        from measured data).
    weighted_hist       - weighted histogram incorporating the uncertainties


Functions:

    def get_lowest_airmass(dirname):
    def get_executable(binname):
    def divideFits(file1, file2, outputfile="ratio.fits", file=True,
    def ZP(etime):
    def getAllData(fitsfile, ap=None):
    def runningAverage(data, binwidth):
    def match(x1, y1, x2, y2, tol=0.01, verb=False):
    def binWithErrors(lcdata, errors, npts):
    def correctForAirmass(lightcurveData, airmassData):
    def rotateImage180Degrees(fitsFile):
    def airmass(zdList):
    def moving_average(iterable, n=3):
    def mkdir(newdir):

Classes:

    class printStatus(object):
    class NGTSFilenameParser(object):
    class AirmassControl(object):
    class SavGol(object):
    class bcolours:
    class clippedMean(object):
    class airmassLookupTable(object):
    class Dir(object):
    class fitsDir(Dir):

srw.psf
----------

Very specific to NGTS, this object manages a "Aperture profile" (similar to
the psf but using an aperture to measure the profile, making it not a
theoretical psf but an observed psf). Therefore it is not so useful for
most people.

srw.photom
----------

For performing aperture photometry on a single target from a Python
script.

srw.NOMADParser
---------------

Querying the NOMAD catalogue (Johnson-Cousins magnitudes, positions and
proper motions) within a cone of radius r arcminutes at position
(ra,dec).


"""

import numpy as np
#import exceptions
import os
from subprocess import Popen, PIPE, STDOUT
import sys
from contextlib import contextmanager
#from itertools import islice
#from collections import deque
#from UserDict import UserDict
#import pyfits
#from slalib import sla_airmas
#from jg.subs import progressbarClass
#from subs import bin, wav
#import matplotlib.pyplot as plt

#class UCACParser(object):
    #"""
    #Object to parse the UCAC Catalogue to get a list of I band magnitudes
    #and radial distances
    #"""
    #imag = []
    #dist = []
    #ralist = []
    #declist = []
    #def __init__(self, ra, dec, radius, maxnum=9999, maglow=19., maghigh=10.):
        #"""
        #Set up initial variables, radius in arcmin
        #"""
        #super(UCACParser, self).__init__()

        #self.ra = ra
        #self.dec = dec
        #self.radius = radius
        #self.maxnum = maxnum
        #self.maglow = maglow
        #self.maghigh = maghigh

    #def __imag__(self, line):
        #"""
        #Internal function

        #Given a line, return the I magnitude value
        #"""
        #return line[221:].split(":")[0]

    #def __dist__(self, line):
        #"""
        #Internal function

        #Given a line, return the distance value
        #"""
        #return line[259:].strip()

    #def __radec__(self, line):
        #"""
        #Internal function

        #Given a line, return the ra and dec
        #"""
        #import re
        #try:
            #radecstring = line.split("|")[1].split()[0]
        #except IndexError:
            #pass
        #else:
            #regex = re.compile(r'(?P<ra>\d+\.\d+)(?P<dec>[+-]\d+\.\d+)')
            #match = regex.search(radecstring)
            #if match:
                #return float(match.group('ra')), float(match.group('dec'))

    #def get(self):
        #"""
        #Retrieve the data from the UCAC server
        #"""
        #cmd = " ".join([
            #"finducac3",
            #str(self.ra),
            #str(self.dec),
            #"-r",
            #str(self.radius),
            #"-lmi",
            #"%.2f,%.2f" % (self.maghigh, self.maglow),
            #"-m",
            #str(self.maxnum),
            #])
        #p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        #self.data, err = p.communicate()
        #if 'command not found' in err:
            #raise OSError(err)
        #return self

    #def parse(self):
        #"""
        #Parse the UCAC data
        #"""
        #for line in self.data.split("\n"):
            #if "#" in line:
                ##comment string
                #pass
            #else:
                #try:
                    #self.imag.append(float(self.__imag__(line)))
                    #self.dist.append(float(self.__dist__(line)))
                    #raval, decval = self.__radec__(line)
                    #self.ralist.append(raval)
                    #self.declist.append(decval)
                #except ValueError:
                    #pass

        #assert len(self.imag) == len(self.dist)
        #assert len(self.ralist) == len(self.imag)
        #assert len(self.declist) == len(self.ralist)
        #return self

    #def __str__(self):
        #return "UCAC Parser, %d objects found" % (len(self.imag),)


@contextmanager
def shelve_open(fname, *args, **kwargs):
    '''
    Shelve is a wrapper around cPickle to store pickable data in a dictionary
    type object on disk.

    The opening code does not have context manager ability so this function
    handles opening the database file and closing it again with context.
    '''
    import shelve
    db = shelve.open(fname, *args, **kwargs)
    yield db
    db.close()

def get_lowest_airmass(dirname):
    """
    Function takes a directory name and reads the airmass information
    from all the fits files held within, and returns the filename
    of the file with the lowest airmass
    """
    import srw
    import pyfits
    fdir = srw.fitsDir(dirname)
    airmass = []
    for file in fdir.files():
        header = pyfits.getheader(file)
        airmass.append(float(header['airmass']))

    airmass = np.array(airmass)
    return np.array(fdir.files())[airmass==airmass.min()][0]

def get_executable(binname):
    """Gets full path for the executable given for
    python"""
    pathvar = os.environ['PATH']
    dirs = pathvar.split(":")


    for dir in dirs:
        if os.path.exists(dir):
            files = os.listdir(dir)
            if binname in files:
                return os.path.join(dir, binname)

    raise OSError("Binary '%s' not found" % binname)

class printStatus(object):
    """Nice formatting for status messages. Alows a simple progress
    counter coloured nicely, and a status message.

    Create the object before any loop with the total number of iterations,
    and a prefix message that will be printed in every call."""
    def __init__(self, total, prefix="", colour=True,
            stream=sys.stdout):
        """Constructor function. Arguments are:

        total - the total number of iterations (required)
        prefix - any message that will be printed on every call to the update
                    function
        colour - do you want the status messages in colour? Set to false to
                    disable this behaviour (eg. if your terminal does not
                    understand asci colour codes)
        stream - can be file object if you want a log, otherwise usually will
                    be sys.stdout or sys.stderr."""
        super(printStatus, self).__init__()

        self.total = total
        self.prefix = prefix
        self.colour = colour
        self.stream = stream
        self.count = 1
        self.cols = bcolours()
        if not self.colour:
            self.cols.disable()

    def progress(self, string="", number=None, newline=True):
        """Update the progress counter. The prefix message will be printed,
        followed by any text passed to this function in the string argument.
        If newline is False, no newline will be printed (for use with the
        postcmd function)"""
        if number == None:
            if newline:
                print >> self.stream, self.cols.HEADER + "[" + self.cols.OKBLUE + str(self.count) + \
                        "/" + str(self.total) + self.cols.HEADER + "] " + self.cols.OKGREEN + \
                        self.prefix.strip() + " " + string.strip() + self.cols.ENDC
            else:
                print >> self.stream, self.cols.HEADER + "[" + self.cols.OKBLUE + str(self.count) + \
                        "/" + str(self.total) + self.cols.HEADER + "] " + self.cols.OKGREEN + \
                        self.prefix.strip() + " " + string.strip() + self.cols.ENDC,

            self.count += 1

        else:
            if newline:
                print >> self.stream, self.cols.HEADER + "[" + self.cols.OKBLUE + str(number) + \
                        "/" + str(self.total) + self.cols.HEADER + "] " + self.cols.OKGREEN + \
                        self.prefix.strip() + " " + string.strip() + self.cols.ENDC
            else:
                print >> self.stream, self.cols.HEADER + "[" + self.cols.OKBLUE + str(number) + \
                        "/" + str(self.total) + self.cols.HEADER + "] " + self.cols.OKGREEN + \
                        self.prefix.strip() + " " + string.strip() + self.cols.ENDC,

    def postcmd(self, string):
        """For printing another message after the progress message, for
        example printing 'Done'. Best combined with the newline=False
        argument for the progress method."""
        print >> self.stream, self.cols.HEADER + string + self.cols.ENDC



def divideFits(file1, file2, outputfile="ratio.fits", file=True,
        clobber=True):
    """Reads in data from the two files, calculates
    the ratio of the two primary image arrays. The ratio array
    is always returned but optionally the array can be written to a
    new fits file"""
    import pyfits
    data1 = pyfits.getdata(file1)
    data2 = pyfits.getdata(file2)

    ratio = data1 / data2
    if file:
        pyfits.HDUList([
            pyfits.PrimaryHDU(ratio),
            ]).writeto(outputfile, clobber=clobber)
        return ratio
    else:
        return ratio


class NGTSFilenameParser(object):
    """Parses NGTS filenames for information about
    the date, meridian, aperture size and what
    postprocessing has gone on"""
    filename = None
    date = None
    meridian = None
    apsize = None
    pprocess = None
    tamuz = False
    stip = False
    crfilt = False

    def __init__(self, filename):
        """Main function, requires filename argument"""
        import re
        super(NGTSFilenameParser, self).__init__()

        self.filename = filename

        datematch = re.search(r"(?P<date>^20\d{6})",
                self.filename)
        if datematch:
            self.date = datematch.group("date")

        meridianmatch = re.search(r"(?P<meridian>[EW][ea]st)",
                self.filename)
        if meridianmatch:
            self.meridian = meridianmatch.group("meridian")

        apsizematch = re.search(r"ap(?P<apsize>\d+\.\d+)",
                self.filename)
        if apsizematch:
            self.apsize = float(apsizematch.group('apsize'))

        postprocessmatch = re.search(
                r"^.*ap\d+\.\d+(?P<extra>.*)\.fits$",
                self.filename)

        if postprocessmatch:
            extras = postprocessmatch.group("extra").split("_")
            upperextras = [x.upper() for x in extras]
            if "TAMUZ" in upperextras:
                self.tamuz = True
            if "STR" in upperextras:
                self.strip = True
            if "CRFILT" in upperextras:
                self.crfilt = True

class AirmassControl(object):
    """Takes airmass information from fits files, or if
    the information already exists in cache, reads from
    there.

    The object requires a 'datestr' argument which is the
    date and meridian of the night in the format:

        - YYYYMMDDm

    where Y is a year digit, M the month, D the day and m
    the meridian: either East or West or left blank if
    there is no meridian."""
    import os.path
    def __init__(self, filename):
        """Initialise data, arguments handled"""
        super(AirmassControl, self).__init__()

        self.filename = os.path.basename(filename)
        self.cache_dir = os.path.expanduser("~/work/phd/AirmassCatalogue")
        self.data_dir = os.path.expanduser("~/work/phd/split_data")

        self.filenames()

        if os.path.isfile(self.fullcachepath):
            self.update_from_cache()
        else:
            self.update_from_data()

    def filenames(self):
        #upperdatestr = self.datestr.upper()
        fnameparser = NGTSFilenameParser(self.filename)
        #if "EAST" in upperdatestr or "E" in upperdatestr:
        if fnameparser.meridian:
            if fnameparser.meridian.upper() == "EAST":
                self.fulldatapath = os.path.join(
                        self.data_dir,
                        fnameparser.date,
                        "EastMeridian",
                        )
                self.fullcachepath = os.path.join(
                        self.cache_dir,
                        fnameparser.date + "E.am.npy",
                        )
            #elif "WEST" in upperdatestr or "W" in upperdatestr:
            elif fnameparser.meridian.upper() == "WEST":
                self.fulldatapath = os.path.join(
                        self.data_dir,
                        fnameparser.date,
                        "WestMeridian",
                        )
                self.fullcachepath = os.path.join(
                        self.cache_dir,
                        fnameparser.date + "W.am.npy",
                        )
        else:
            self.fulldatapath = os.path.join(
                    self.data_dir,
                    fnameparser.date,
                    )
            self.fullcachepath = os.path.join(
                    self.cache_dir,
                    fnameparser.date + ".am.npy",
                    )



    def update_from_data(self):
        from pyfits import getheader
        # need to check there are no extra East/West subdirectories
        allfiles = os.listdir(self.fulldatapath)
        valid = True
        if "EastMeridian" in allfiles or "WestMeridian" in allfiles:
            raise RuntimeWarning("Directory contains valid subdirectories, "\
                    "perhaps you mean to include these. Not caching data"
                    )
            valid = False
        fdir = fitsDir(self.fulldatapath)
        self.airmass = []
        for f in fdir:
            self.airmass.append(
                    getheader(f)['airmass'],
                    )

        self.airmass = np.array(self.airmass)
        if valid:
            np.save(
                    self.fullcachepath,
                    self.airmass,
                    )





    def update_from_cache(self):
        self.airmass = np.load(self.fullcachepath)





def ZP(etime):
    """Zero point for NGTS data scaled by exposure time.

    Decided not to include a default value as this will cause
    troubles.

    Derivation
    ----------

    If f is the base flux and f' the flux at a greater exposure time,
    for the two measurements to give the same zero point, the equation

    m = m_0,1 - 2.5*log10(f) = m_0,2 - 2.5*log10(f')

    m_0,2 - m_0,1 = 2.5*log10(f') - 2.5*log10(f)
                  = 2.5*[log10(f') - log10(f)]
                  = 2.5*log10(f' / f)

    if f' = t*f where t is the ratio of exposure times then

    m_0,2 - m_0,1 = 2.5*log10(t)

    so m_0,2 = m_0,1 + 2.5*log10(t)

    """
    zp_40 = 24.51
    ref_exp = 40.
    zp = zp_40 + 2.5*np.log10(float(etime) / ref_exp)

    return zp

#def fasper(x, y, err, nf=0, fmax=0., pgram=True):
    #"""Wrapper around Tom's code for the result to make sense"""
    #from trm.subs import fasper as trm_fasper
    #freq, power = trm_fasper(x, y, err, nf, fmax, True)
    #if pgram:
        #return freq, power
    #else:
        #expected_variance = np.sqrt(np.average(err**2))**2
        #power *= expected_variance
        #return freq, np.sqrt(power)


def getAllData(fitsfile, ap=None):
    """Gets all useful data from a fits file (flux, jd, err).
    ap argument allows specifying which aperture to extract"""
    import pyfits
    f = pyfits.open(fitsfile)
    if ap:
        flux = f['flux'].data[ap, :]
        jd = f['hjd'].data[ap, :]
        err = f['fluxerr'].data[ap, :]
    else:
        flux = f['flux'].data
        jd = f['hjd'].data
        err = f['fluxerr'].data

    f.close()

    return {"flux": flux,
            "jd": jd,
            "err": err
            }

class SavGol(object):
    """Savitzky-Golay filter object"""
    # set up member variables
    coeff = None


    def calc_coeff(self, num_points, pol_degree, diff_order=0):
        """ calculates filter coefficients for symmetric savitzky-golay filter.
        see: http://www.nrbook.com/a/bookcpdf/c14-8.pdf

        num_points   means that 2*num_points+1 values contribute to the
                     smoother.

        pol_degree   is degree of fitting polynomial

        diff_order   is degree of implicit differentiation.
                     0 means that filter results in smoothing of function
                     1 means that filter results in smoothing the first
                                                 derivative of function.
                     and so on ...

    """

        # setup interpolation matrix
        # ... you might use other interpolation points
        # and maybe other functions than monomials ....

        x = np.arange(-num_points, num_points+1, dtype=int)
        monom = lambda x, deg : pow(x, deg)

        A = np.zeros((2*num_points+1, pol_degree+1), float)
        for i in range(2*num_points+1):
            for j in range(pol_degree+1):
                A[i,j] = monom(x[i], j)

        # calculate diff_order-th row of inv(A^T A)
        ATA = np.dot(A.transpose(), A)
        rhs = np.zeros((pol_degree+1,), float)
        rhs[diff_order] = (-1)**diff_order
        wvec = np.linalg.solve(ATA, rhs)

        # calculate filter-coefficients
        self.coeff = np.dot(A, wvec)

    def smooth(self, signal):
        """ applies coefficients calculated by calc_coeff()
            to signal """
        if self.coeff == None:
            raise AttributeError("Call calc_coeff before trying to smooth data")

        N = np.size(self.coeff-1)/2
        res = np.convolve(signal, self.coeff)
        return res[N:-N]





#def Hamming(length):
    #"""Generate a Hamming window of length length"""
    #data = []
    #for i in range(length):
        #data.append(0.54 - 0.46*np.cos(2.*np.pi*i/(length-1)))
    #return np.array(data)

class bcolours:
    """A class of fancy terminal colours

    Instanciate (cannot spell that...) this class
    and print the member variables to change the
    colours. Also enables turning off the colours
    if the disable function is called"""

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        """Disable the fancy colour printing"""
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

#class pbClass(progressbarClass):
    #"""Custom class with new init function to enable writing
    #to stderr or any custom filestream"""
    #def __init__(self, finalcount, stream=sys.stdout):
        #"""Create the file and write the bar to screen"""
        #self.finalcount=finalcount
        #self.blockcount=0
        #self.block="*"
        #self.f=stream
        ##
        ## If the final count is zero, don't start the progress gauge
        ##
        #if not self.finalcount : return
        #self.f.write('\n------------------ % Progress -------------------1\n')
        #self.f.write('    1    2    3    4    5    6    7    8    9    0\n')
        #self.f.write('----0----0----0----0----0----0----0----0----0----0\n')


def runningAverage(data, binwidth):
    """Function takes two arguments, the data itself in numpy.array form
    and the number of points in each bin. It returns the raw data truncated
    to be the same length as the binned data, and the binned data, allowing
    for easy division of one by the other."""
    #simple type checking
    if type(data) != np.ndarray:
        raise TypeError("Argument must be array")

    lc = [] #return array
    for i in range(binwidth/2, len(data) - binwidth/2):
        if binwidth % 2: # odd case
            lc.append(sum(data[i-binwidth/2:i+1+binwidth/2]) / float(binwidth))
        else:
            lc.append(sum(data[i-binwidth/2:i+binwidth/2]) / float(binwidth))



    return np.array(data[binwidth/2:len(data)-binwidth/2]), np.array(lc)


def match(x1, y1, x2, y2, tol=0.01, verb=False):
    """Function takes two lists (either xy or ra/dec in decimal)
and returns a list, each element a tuple of x1, y1, x2, y2 that match"""
    #declare output lists
    outx1=[]
    outx2=[]
    outy1=[]
    outy2=[]

    count = 0 # how many were matched

    for i, line in enumerate(zip(x2, y2)):   #for every line in the second list
        try:
            tmpra = np.ones(x1.shape)*line[0]
        except AttributeError:
            tmpra = np.ones(len(x1))*line[0]

        try:
            tmpdec = np.ones(y1.shape)*line[1]
        except AttributeError:
            tmpdec = np.ones(len(y1))*line[1]

        raind = np.where(abs(x1-tmpra)/tol<1.)[0]
        decind = np.where(abs(y1-tmpdec)/tol<1.)[0]
        if raind.size and decind.size:
            for ind in raind:
                if ind in decind:
                    #result.append((x1[ind], y1[ind], x2[i], y2[i]))
                    outx1.append(x1[ind])
                    outy1.append(y1[ind])
                    outx2.append(x2[i])
                    outy2.append(y2[i])
                    count += 1

    if verb:
        print count, "apertures matched"
    return outx1, outy1, outx2, outy2

#class rmsVsBrightness(object):
    #"""
#Object to take a data set and produce an rms vs
#brightness plot

#Optionally takes a np.ndarray argument
    #"""
    #def __init__(self, data=None, sky=None, thresh=1E5, bins=30, fname=None):
        #object.__init__(self)

        ## make sure the arguments (if they exist) are of
        ##   the right type
        #t = type(data)

        #if not t:
            #self._data = None
        #elif t == np.ndarray or t == np.array:
            #self._data = data
        #elif t == list:
            #self._data = np.array(data)
        #else:
            #self._data = None

        #self._fname = fname

        #t = type(sky)

        #if not t:
            #self._sky = None
        #elif t == np.ndarray or t == np.array:
            #self._sky = sky
        #elif t == list:
            #self._sky = np.array(sky)
        #else:
            #self._sky = None

        #self._bins = bins
        #self._thresh = thresh

        ##set up variables
        #self._averages = []
        ##self._rmsDeviations = []
        ##self._medianable = []

        #return None

    #def update_data(self, data):
        #self._data = data

    #def update_sky(self, sky):
        #self._sky = data

    #def update_thresh(self, thresh):
        #self._thresh = thresh

    #def update(self, data=None, sky=None, thresh=None):
        #if data:
            #self.update_data(data)
        #if sky:
            #self.update_sky(sky)
        #if thresh:
            #self.update_thresh(thresh)

    #def calculateVariables(self):
        ## transpose data
        #try:
            #self._data = self._data.T
        #except:
            #print >> sys.stderr, "Error transposing data information"
        #try:
            #self._sky = self._sky.T
        #except AttributeError:
            #pass

        #self._averages = np.average(self._data, axis=0)
        #self._rms =  np.std(self._data, axis=0)
        #self._rmsDeviations = self._rms / self._averages
        #self._medianable = self._rmsDeviations[self._averages > self._thresh]
        #self._med = np.median(self._medianable)
        #self._sd = np.std(self._medianable)
        #self._mad = np.median(np.abs(self._medianable - self._med))

        #try:
            #self._theoreticalValues = calculateNoise(self._data, self._sky)
        #except TypeError:
            #pass

        #return self._averages, self._rms, self._rmsDeviations, self._medianable, self._med,\
                #self._sd, self._mad


    #def plot(self):
        #import matplotlib.pyplot as plt
        ##get data
        #self.calculateVariables()

        ##plotting section
        #logLogRect = [0.1, 0.4, 0.8, 0.5]
        #histRect = [0.1, 0.1, 0.8, 0.2]

        #fig = plt.figure(figsize=(8, 10))
        #ax = fig.add_axes(logLogRect)
        #ax.plot(self._averages, self._rmsDeviations, 'bx', label='Data')
        #ax.axhline(self._med, color='r', label='Median', alpha=0.3)

        ##if there is no sky argument
        #try:
            #ax.plot(self._theoreticalValues[0], self._theoreticalValues[1], 'g.', alpha=0.3)
        #except AttributeError:
            #pass

        #ax.set_xlabel('Average brightness')
        #ax.set_ylabel('Fractional RMS')
        #ax.set_xscale('log')
        #ax.grid(which='minor', alpha=0.3)
        #ax.grid(which='major', alpha=0.5)
        #ax.set_yscale('log')


        ##a = np.array([10, axisLimits[1]])
        #ax.axis([10, 1E6, 1E-3, 1E2])

        #histAx = fig.add_axes(histRect)
        #h = histAx.hist(self._medianable, self._bins,
                #(self._med-2*self._mad,self._med+2*self._mad))

        #histAx.set_title('Histogram of values above threshold')
        #histAx.set_xlabel('Fractional RMS')
        #histAx.set_ylabel('Frequency (%d bins)' % (self._bins,))
        #histAx.axvline(self._med, color='r')

        #histAx.text(0.9, 0.9,'%d/%d' % (len(self._medianable), self._data.shape[0]),
         #horizontalalignment='center',
         #verticalalignment='center',
         #transform = histAx.transAxes)

        #ind = np.where(h[0]==h[0].max())[0][0]#.toList().index([True])
        #center = (h[1][ind] + h[1][ind+1]) / 2.
        #ax.axhline(center, color='m', label='Mode', alpha=0.3)
        ##histAx.axvline(center, color='m')
        ##histAx.axhline(clippedMean, color='m')

        #if not self._fname:
            #ax.set_title('''RMS of deviations vs. brightness
    #Median: %f, Mode: %f''' % (self._med, center))
        #else:
            #ax.set_title("""RMS of deviations vs. brightness for file %s
    #Median: %f, Mode: %f""" % (self._fname, self._med, center))
        #ax.legend()

        #plt.show()




#def overlayRmsPlots(mainTitle, *rmsPlots):
    #fig = plt.figure(figsize=(12, 12))
    #ax = fig.add_subplot(111)

    #markers = ['o', 'x', '.']
    #linestyles = ['-', '--', ':']
    #alphas = [0.3, 0.5, 1.0]

    #for i, data in enumerate(rmsPlots):
        #vals = rmsVsBrightness(data[0]).calculateVariables()
        #title = data[1]
        #ax.plot(vals[0], vals[2], markers[i], alpha=alphas[i], ls='None', label=title.capitalize())
        #ax.axhline(vals[4], ls=linestyles[i], label='Median %f %s' % (vals[4], title))

    #ax.axvline(1E5, label='Threshold', alpha=0.3)

    #ax.set_xscale('log')
    #ax.set_yscale('log')

    #ax.grid(which='minor', alpha=0.2)
    #ax.grid(which='major', alpha=0.3)

    #ax.legend(loc='best')

    #ax.set_title(mainTitle)
    #ax.set_ylabel('Fractional rms brightness')
    #ax.set_xlabel('Average brightness')




    #return fig





























#def calculateNoise(signalData, noiseData):
    #"""returns an array of points with theoretical
    #signal to noise values"""





    #rScience = 2.0
    #rSkyInnerScale = 1.77
    #rSkyOuterScale = 3.79
    #rSkyInner = rScience * rSkyInnerScale
    #rSkyOuter = rScience * rSkyOuterScale
    #aSkyInner = rSkyInner**2 * np.pi
    #aSkyOuter = rSkyOuter**2 * np.pi
    #aSky = aSkyOuter - aSkyInner
    ##skyPerPix = noiseData / skyPixels # get sky counts per pixel


    #aScience = rScience**2 * np.pi




    ##Na = signalData
    ##Nb = aScience * skyPerPix
    ##Ns = Na + Nb

    #mirrorTerm = 1. + aScience / aSky
    ##noise = np.sqrt(signalData + aScience * mirrorTerm * ((noiseData / aSky) + 7.83**2))
    #noise = np.sqrt(signalData + aScience * ((noiseData / aSky) + 7.83**2))

    ##flattenedAverage = np.average(Ns, axis=0)

    ##sqrts = np.sqrt(Ns + Nb)
    ##flattenedRMS = np.average(sqrts, axis=0)
    #avSignal = np.average(signalData, axis=0)
    #avNoise = np.average(noise, axis=0) / avSignal



    ## remove any nans
    ##ind = np.where(flattenedRMS != flattenedRMS)[0]
    ##flattenedFractionalError = np.delete(flattenedRMS, ind) / np.delete(flattenedAverage, ind)

    ##return np.delete(flattenedAverage, ind), flattenedFractionalError
    #return avSignal, avNoise


#def bin(lcdata, npts, verbose=False, algorithm="median"):
    #"""
#Function takes a 1d array of data and performs
#a simple bin routine, averaging up each
#npts values and returns a new array with these values.

#No typechecking currently takes place
    #"""

    #data = []
    #filteredData = np.array(lcdata)


    #endpoint = filteredData.shape[0]
    #loopCount = 0
    #while filteredData.shape[0] % npts:
        #endpoint -= 1
        #filteredData = filteredData[:endpoint]
        #loopCount += 1

    #if loopCount and verbose:
        #print  >> sys.stderr, "Had to subtract %d points from the end" % (loopCount,)



    #i = 0
    ##while i < filteredData.shape[0]:
    #for i in range(0, filteredData.shape[0], npts):
        #sum = 0
        ##for j in range(npts):
        ##    sum += filteredData[i+j]

        ##sum /= float(npts)

        #if algorithm.upper() == "MEDIAN":
            #data.append(np.median(filteredData[i:i+npts]))
        #elif algorithm.upper() == "AVERAGE":
            #data.append(np.average(filteredData[i:i+npts]))

        ##i += npts

    #return  np.array(data)

def binWithErrors(lcdata, errors, npts):
    """
Function takes a 1d array of data, a 1d array
of error data and number of points per bin
data and returns 2 lists:

    The binned array
    The errors added in quadrature

No typechecking currently takes place
    """

    oplcdata = []
    opedata = []
    filteredData = lcdata


    endpoint = filteredData.shape[0]
    loopCount = 0
    while filteredData.shape[0] % npts:
        endpoint -= 1
        filteredData = filteredData[:endpoint]
        loopCount += 1

    filteredErrors = errors[:endpoint]

    if loopCount:
        print  >> sys.stderr, "Had to subtract %d points from the end" % (loopCount,)


    i = 0
    while i < filteredData.shape[0]:
        sum = 0
        totErr = 0
        for j in range(npts):
            sum += filteredData[i+j]
            totErr += filteredErrors[i+j]**2

        sum /= float(npts)
        totErr = np.sqrt(totErr)

        oplcdata.append(sum)
        opedata.append(totErr)
        i += npts

    return  np.array(oplcdata), np.array(opedata)

#def WASP2JD(wasp):
    #"""
#Converts between WASP date and JD

#JD = (WASP/86400.) + 2453005.5

#"""

    #const = 2453005.5
    #secInDay = 86400.

    #if type(wasp) == list:
        #wasp = np.array(wasp)
    #elif type(wasp) == tuple:
        #wasp = np.array(list(wasp))
    #elif type(wasp) == np.ndarray or type(wasp) == np.array:
        #pass
    #elif type(wasp) == float or type(wasp) == int:
        #pass

    #try:
        #return (wasp / secInDay) + const
    #except TypeError:
        #return (float(wasp) / secInDay) + const




class clippedMean(object):
    """Calculates clipped mean for the array given

    Usage:
    Allocate a new `clippedMean` object, with a initial argument of the
    data, and a tuple/list of sigma multiples outside of which the
    data will be rejected.

    * To return the clipped mean value, call :func:`average`
    * To return the filtered data, call :func:`data`
    * To return the history of the statistics call :func:`history`
    """
    def __init__(self, data, sigma=(5, 3)):
        '''Set up initial data'''
        object.__init__(self)
        if type(data) == np.ndarray or type(data) == np.array:
            self._data = data
            self._sd = np.std(self._data, axis=0)
            self._av = np.average(self._data, axis=0)
        elif type(data) == list or type(data) == tuple:
            try:
                self._data = np.array(data)
            except ValueError:
                raise ValueError('Argument must be rectangular array')
            self._sd = np.std(self._data)
            self._av = np.average(self._data)
        else:
            raise TypeError("Incorrect data type for 'data' argument")

        self._sigma = sigma
        self._newData = self._data

        self._avHistory = []
        self._sdHistory = []

        for val in self._sigma:
            self._flatten(val)
            self._avHistory.append(self._av)
            self._sdHistory.append(self._sd)


        return None

    def _flatten(self, sigma):
        ind = (self._newData < self._av + sigma * self._sd) & \
                (self._newData > self._av - sigma * self._sd)
        self._newData = self._newData[ind]
        self._sd = np.std(self._newData)
        self._av = np.average(self._newData)


    def data(self):
        """Returns the filtered data

        This method does not keep the positions, or set rejected
        data to any value so any coherence with other data sets
        is lost"""
        return self._newData

    def average(self):
        """Returns the clipped mean value"""
        return self._av

    def history(self, avs=True, sds=True):
        """Returns the history of averages and standard deviations
        (for debugging purposes mostly)"""
        if avs and sds:
            return self._avHistory, self._sdHistory
        elif avs and not sds:
            return self._avHistory
        elif sds and not avs:
            return self._sdHistory
        else:
            return None

#class AirmassError(exceptions.Exception):
    #"""Custom exception for airmass"""
    #def __init__(self, value):
        #self.value = value
    #def __str__(self):
        #return self.value

def correctForAirmass(lightcurveData, airmassData):
    """
Corrects for airmass data based on zenith distance
information

* First argument should be an array-type of lightcurve data
* Second argument should be an array-type of zd data
* First mags are calculated with m=-2.5log10(f)
* Then linear fit is found against airmass

* Following this, each flux value has a correction value calculated
    from substituting the airmass value in the fit found
* Correction value is subtracted from the original lightcurve value
* This array is returned

Raises a custom error if fit has NaN's
    """
    # Type checking
    if type(lightcurveData) == np.array or type(lightcurveData) == np.ndarray:
        pass
    elif type(lightcurveData) == list:
        lightcurveData = np.array(lightcurveData)
    else:
        raise TypeError('Argument must be array-type')

    if type(airmassData) == np.array or type(airmassData) == np.ndarray:
        pass
    elif type(airmassData) == list:
        airmassData = np.array(airmassData)
    else:
        raise TypeError('Argument must be array-type')

    # Get average
    av = np.average(lightcurveData)

    # Calculate mags
    mags = -2.5 * np.log10(lightcurveData)

    # check for invalid mag values

    if (mags != mags).any():
        raise AirmassError('Mags has NaN value')
    elif (mags == float('inf')).any():
        raise AirmassError('Mags has inf value')


    # Calculate linear fit
    fit = np.poly1d(np.polyfit(airmassData, mags, 1))

    # check for NaNs
    if fit.c[0] != fit.c[0]:
        raise AirmassError('Gradient has NaN value')
    if fit.c[1] != fit.c[1]:
        raise AirmassError('Intercept has NaN value')

    newLightcurveData = []

    for l, a in zip(lightcurveData, airmassData):
        magCorr = fit(a)
        fluxCorr = 10**(- magCorr / 2.5)
        newLightcurveData.append(l - fluxCorr)


    return np.array(newLightcurveData) + av

#def subtractFirstOrderLinear(lightcurveData):
    #"""
#Corrects for airmass data based on a purely statistical
#method

#* Subtracts average
#* fits linear fit to residuals
#* Subtracts fit of data
#* Returns numpy array of result
    #"""

    ## Type checking

    #if type(lightcurveData) == np.array or type(lightcurveData) == np.ndarray:
        #pass
    #elif type(lightcurveData) == list:
        #lightcurveData = np.array(lightcurveData)
    #else:
        #raise TypeError('Argument must be array-type')

    ## generate residuals
    #residuals = lightcurveData - np.average(lightcurveData)

    ## perform linear fit
    #x = np.arange(residuals.shape[0])
    #airmassFit = np.poly1d(np.polyfit(x, residuals, 1))

    #return lightcurveData - airmassFit(x)

def rotateImage180Degrees(fitsFile):
    """
Rotates fits file by 180 degrees and updates
original file
    """
    try:
        f = pyfits.open(fitsFile, mode='update')
    except:
        print >> sys.stderr, "Error opening file %s" % fitsFile
        sys.exit(1)

    try:
        data = f[0].data
    except IndexError:
        print >> sys.stderr, "Error: File must be fits file"
        sys.exit(1)


    # Rotate data twice
    data = np.rot90(data)
    data = np.rot90(data)

    # Update original file
    f[0].data = data

    # Flush the buffer
    f.flush()

    f.close()


def airmass(zdList):
    """
takes list of radians values and returns list of
airmass values
    """
    if type(zdList) == list:
        pass
    elif type(zdList) == np.ndarray or type(zdList) == np.array:
        if zdList.ndim != 1:
            print >> sys.stderr, "Single dimension array required"
            sys.exit(1)
    else:
        print >> sys.stderr, "List type argument required"
        sys.exit(1)

    airmassArray = []
    for val in zdList:
        airmassArray.append(sla_airmas(val))

    return airmassArray





#def deg2rad(degVal):
    #"""Converts array/single value of degrees to radians"""
    #if type(degVal) == np.ndarray or type(degVal) == np.array:
        ##numpy condition
        #pass
    #elif type(degVal) == list:
        #degVal = np.array(degVal)
    #else:
        #try:
            #degVal = float(degVal)
        #except ValueError:
            #print >> sys.stderr, "Function takes array type or numerical argument"
            #sys.exit(1)

    #return 2 * np.pi * degVal / 360.

#def rad2deg(radVal):
    #"""Converts array/single value of radians to degrees"""
    #if type(radVal) == np.ndarray or type(radVal) == np.array:
        ##numpy condition
        #pass
    #elif type(radVal) == list:
        #radVal = np.array(radVal)
    #else:
        #try:
            #radVal = float(radVal)
        #except ValueError:
            #print >> sys.stderr, "Function takes array type or numerical argument"
            #sys.exit(1)

    #return 360. * radVal / (2. * np.pi)

#class orderDict(UserDict):
    #"""Custom ordered dictionary class"""
    #def __init__(self, dict=None):
        #self.data = {}
        #if dict:
            #self.update(dict)

    #def sorted(self):
        #"""returns the values sorted by their keys"""
        #try:
            #orderedKeys = sorted([int(key) for key in self.keys()])
        #except ValueError, e:
            #print >> sys.stderr, "Warning: sorted failed due to keys being of incorrect type"
            #return None
        #else:
            #return [self.data[str(key)] for key in orderedKeys]


def moving_average(iterable, n=3):
# moving_average([40, 30, 50, 46, 39, 44]) --> 40.0 42.0 45.0 43.0
# http://en.wikipedia.org/wiki/Moving_average
    it = iter(iterable)
    d = deque(islice(it, n-1))
    d.appendleft(0)
    s = sum(d)
    for elem in it:
        s += elem - d.popleft()
        d.append(elem)
        yield s / float(n)

#class extractSingle(object):
    #"""extracts single amount of data for a lightcurve

    #'Raw' data must be in ascii table form with columns
    #representing a single aperture, and rows being the
    #flux values taken per frame

    #data must have header information with aperture numbers


    #"""
    #def __init__(self, fileName):
        #"""Set up data internally"""
        #super(extractSingle, self).__init__()
        #self._fileName = fileName

        #try:
            #self._file = open(self._fileName)
        #except IOError:
            #print >> sys.stderr, "File %s not found" % self._fileName
            #sys.exit(1)

        #self._header = self._file.readline()
        #self._file.close()

        #self._aps = np.array(self._header.strip().split()[1:])

        #try:
            #self._ldata = np.loadtxt(self._fileName)
        #except IOError:
            #print >> sys.stderr, "File %s not found" % self._fileName
            #exit(1)

    #def numberOfApertures(self):
        #return len(self._aps)

    #def apertures(self):
        #return self._aps

    #def __getitem__(self, item):
        #"""returns data for aperture 'item'

        #called by extractSingle_instance[apno]

        #"""

        #try:
            #assert(str(item) in self._aps)
        #except AssertionError:
            #print >> sys.stderr, "Aperture %s not found in data" % item
            #return None
        #else:
            #return self._ldata[:, str(item)==self._aps][:, 0]

    #def all(self):
        #"""Returns a 2d Numpy array of values"""
        #return self._ldata


#class extractSingleCoords(extractSingle):
    #"""docstring for extractSingleCoords"""
    #def __getitem__(self, item):
        #"""Returns coordinate slice"""
        #try:
            #assert(str(item) in self._aps)
        #except AssertionError:
            #print >> sys.stderr, "Aperture %s not found in data" % item
            #return None
        #else:
            #col = 2 * np.where(str(item)==self._aps)[0][0]
            #return self._ldata[:, col], self._ldata[:, col+1]



class airmassLookupTable(object):
    """
Class to calculate the airmass extinction in mag/airmass per wavelength

Estimated at 50A intervals and interpolated

Credit: RGO/La Palma technical note no 31

Usage:

    Create an airmassLookupTable object, then call the
    extinction(wavelength) method where wavelength is in Angstroms and
    ranges from 3000 to 11000.


    """
    def __init__(self):
        """
Sets up data
        """
        object.__init__(self)
        from scipy.interpolate import interp1d

        #self._wavelength, self._extinction = np.loadtxt('data.csv',
        #        unpack=True, delimiter=',')

        self._wavelength = np.array([
            3000, 3010, 3020, 3030, 3040, 3050, 3060, 3070, 3080, 3090,
            3100, 3110, 3120, 3130, 3140, 3150, 3160, 3170, 3180, 3190,
            3200, 3210, 3220, 3230, 3240, 3250, 3260, 3270, 3280, 3290,
            3300, 3310, 3320, 3330, 3340, 3350, 3360, 3370, 3380, 3390,
            3400, 3410, 3420, 3430, 3440, 3450, 3460, 3470, 3480, 3490,
            3500, 3550, 3600, 3650, 3700, 3750, 3800, 3850, 3900, 3950,
            4000, 4050, 4100, 4150, 4200, 4250, 4300, 4350, 4400, 4450,
            4500, 4550, 4600, 4650, 4700, 4750, 4800, 4850, 4900, 4950,
            5000, 5050, 5100, 5150, 5200, 5250, 5300, 5350, 5400, 5450,
            5500, 5550, 5600, 5650, 5700, 5750, 5800, 5850, 5900, 5950,
            6000, 6050, 6100, 6150, 6200, 6250, 6300, 6350, 6400, 6450,
            6500, 6550, 6600, 6650, 6700, 6750, 6800, 6850, 6900, 6950,
            7000, 7050, 7100, 7150, 7200, 7250, 7300, 7350, 7400, 7450,
            7500, 7550, 7600, 7650, 7700, 7750, 7800, 7850, 7900, 7950,
            8000, 8050, 8100, 8150, 8200, 8250, 8300, 8350, 8400, 8450,
            8500, 8550, 8600, 8650, 8700, 8750, 8800, 8850, 8900, 8950,
            9000, 9050, 9100, 9150, 9200, 9250, 9300, 9350, 9400, 9450,
            9500, 9550, 9600, 9650, 9700, 9750, 9800, 9850, 9900, 9950,
            10000, 10050, 10100, 10150, 10200, 10250, 10300, 10350, 10400, 10450,
            10500, 10550, 10600, 10650, 10700, 10750, 10800, 10850, 10900, 10950,
            11000])

        self._extinction = np.array([
            3.72, 3.31, 2.96, 2.68, 2.42, 2.21, 2.04, 1.9, 1.78, 1.68,
            1.6, 1.52, 1.44, 1.36, 1.27, 1.16, 1.13, 1.09, 1.06, 1.02,
            0.99, 0.96, 0.92, 0.89, 0.86, 0.83, 0.8, 0.77, 0.75, 0.72,
            0.7, 0.69, 0.68, 0.67, 0.66, 0.65, 0.64, 0.63, 0.62, 0.6,
            0.59, 0.58, 0.57, 0.56, 0.56, 0.55, 0.54, 0.53, 0.53, 0.52,
            0.51, 0.48, 0.46, 0.43, 0.41, 0.38, 0.36, 0.34, 0.33, 0.31,
            0.29, 0.28, 0.26, 0.25, 0.24, 0.23, 0.22, 0.21, 0.2, 0.19,
            0.18, 0.17, 0.17, 0.16, 0.15, 0.15, 0.14, 0.14, 0.13, 0.13,
            0.12, 0.12, 0.12, 0.11, 0.11, 0.11, 0.11, 0.11, 0.1, 0.1,
            0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.09, 0.09, 0.09,
            0.09, 0.09, 0.08, 0.08, 0.08, 0.07, 0.07, 0.07, 0.06, 0.06,
            0.06, 0.05, 0.05, 0.05, 0.05, 0.05, 0.04, 0.04, 0.04, 0.04,
            0.04, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
            0.03, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02,
            0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.01, 0.01, 0.01,
            0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
            0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
            0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
            0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
            0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.,
            0.   ])

        self.interpolateFunction = interp1d(self._wavelength,
                self._extinction, kind='cubic')



    def wavelength(self):
        """
Returns a list of the wavelengths as a numpy array
        """
        return self._wavelength

    def extinction(self, wavelength):
        """
Calculate extinction at a wavelength specified (in A)
        """
        return self.interpolateFunction(wavelength)
        #diff = []
        #if wavelength in self._wavelength:
            #return self._extinction[self._wavelength==wavelength][0]
        #else:
            #for val in self._wavelength:
                #diff.append(abs(val - wavelength))


            #minval = min(diff)
            #diff = np.array(diff)

            #return self._extinction[diff==minval][0]


        #ext = np.poly1d(np.polyfit(self._wavelength, self._extinction, 8))
        #from matplotlib.pyplot import *
        #plot(self._wavelength, self._extinction, 'rx')
        #plot(self._wavelength, ext(self._wavelength), 'b-', alpha=0.4)
        #show()
        #return ext(wavelength)

class Dir(object):
    """

Object to handle directories in a nice easy way

Requires argument of path, class will do checking to make sure it exists

"""
    def __init__(self, dir):
        object.__init__(self)

        self._path = os.path.abspath(dir).rstrip('/')
        if os.path.isdir(self._path):
            p = Popen('ls %s/' % self._path, shell=True, stdout=PIPE)
            self._files = [self._path + '/' + val for val in p.communicate()[0].split()]
        elif os.path.isfile(self._path):
            raise OSError('Given path is a file')
        else:
            raise OSError('Given path is not a directory')

        self._filenames = []
        for file in self.files():
            self._filenames.append(os.path.split(file)[1])

    def files(self):
        """returns list of files with absolute paths"""
        return self._files

    def fileNames(self):
        """returns just the filenames without the directories"""
        return self._filenames


    def path(self):
        """returns the path passed to the object"""
        return self._path

    def __str__(self):
        """returns the path passed to the object

x.__str__() <==> str(x)"""
        return self._path

    def __len__(self):
        """returns the number of files

x.__len__() <==> len(x)"""
        return len(self._files)

    def length(self):
        """returns the number of files"""
        return len(self._files)

    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        return self._files.__iter__()

    def __getitem__(self, y):
        """x.__getitem__(y) <==> x[y]"""
        return self._files[y]

    def __delitem__(self, y):
        """x.__delitem__(y) <==> del x"""
        del self._files[y]

    def delbystr(self, string):
        """Deletes any files that include 'string'

Only filenames and not full directory paths"""
        for i, val in enumerate(self._filenames):
            if string in val:
                del self._filenames[i]
                del self._files[i]

#class to deal with a list of fits files
class fitsDir(Dir):
    """

Object to handle fits file directories in a nice easy way

Requires argument of path, class will do checking to make sure it exists

Inherits Dir

"""
    def __init__(self, dir):
        Dir.__init__(self, dir)

        self._path = os.path.abspath(dir).rstrip('/')
        if os.path.isdir(self._path):
            p = Popen('ls %s/*.fits' % self._path, shell=True, stdout=PIPE)
            self._files = p.communicate()[0].split()
        elif os.path.isfile(self._path):
            raise OSError('Given path is a file')
        else:
            raise OSError('Given path is not a directory')

        self._filenames = []
        for file in self.files():
            self._filenames.append(os.path.split(file)[1])

#class dataDir(Dir):
    #"""

#Object to handle data file directories in a nice easy way

#Requires argument of path, class will do checking to make sure it exists

#Inherits Dir

#"""
    #def __init__(self, dir):
        #Dir.__init__(self, dir)

        #self._path = os.path.abspath(dir).rstrip('/')
        #if os.path.isdir(self._path):
            #p = Popen('ls %s/*.cat' % self._path, shell=True, stdout=PIPE)
            #self._files = p.communicate()[0].split()
        #elif os.path.isfile(self._path):
            #raise OSError('Given path is a file')
        #else:
            #raise OSError('Given path is not a directory')

        #self._filenames = []
        #for file in self.files():
            #self._filenames.append(os.path.split(file)[1])

#class Aperture(object):
    #"""Aperture object"""
    #def __init__(self, num):
        #"""Create lists and values"""
        #object.__init__(self)
        #self.num = num
        #self.xcoord = []
        #self.ycoord = []
        #self.sky = []
        #self.flux = []
        #self.errors = []
        #self.mag = []

    #def addLine(self, line):
        #"""Add data into object in the form:
            #xcoordinate
            #ycoordinate
            #sky value
            #flux value
            #mag-errors
            #magniture"""
        #self.xcoord.append(float(line[0]))
        #self.ycoord.append(float(line[1]))
        #self.sky.append(float(line[2]))
        #self.flux.append(float(line[3]))
        #self.errors.append(float(line[4]))
        #self.mag.append(float(line[5]))


    #def coords(self):
        #"""Returns the coordinates in a 2-tuple"""
        #return self.xcoord, self.ycoord

    #def getErrors(self):
        #"""Returns the errors on the flux, as scaled
        #by the errors in magnitude"""
        #ar_err = np.array(self.errors)
        #ar_mag = np.array(self.mag)
        #ar_flux = np.array(self.flux)


        #return list(ar_flux * ar_err / ar_mag)

    #def xyscatter(self):
        #"""Returns 2-tuple of x and y offsets from the average
        #coordinate"""
        #x = np.array(self.xcoord)
        #y = np.array(self.ycoord)
        #self.xav = np.average(self.xcoord) * np.ones(x.shape)
        #self.yav = np.average(self.ycoord) * np.ones(y.shape)

        #return list(x - self.xav), list(y - self.yav)


    #def stats(self):
        #"""Returns the average and standard deviation"""
        #return np.average(self.flux), np.std(self.flux)

    #def residuals(self):
        #av = self.stats()[0] * np.ones(np.shape(self.flux))
        #return self.flux - av

    #def smoothListGaussian(self, degree=2):
        #"""Gaussian smoothing function
        #credit: http://www.swharden.com/blog/2008-11-17-linear-data-smoothing-in-python/"""
        #window=degree*2-1
        #weight=np.array([1.0]*window)
        #weightGauss=[]
        #for i in range(window):
            #i=i-degree+1
            #frac=i/float(window)
            #gauss=1/(np.exp((4*(frac))**2))
            #weightGauss.append(gauss)
        #weight=np.array(weightGauss)*weight
        #smoothed=[0.0]*(len(self.flux)-window)
        #for i in range(len(smoothed)):
            #smoothed[i]=sum(np.array(self.flux[i:i+window])*weight)/sum(weight)
        #return smoothed


    ##end of class definition


#def aperComp(aplist):
    #"""Arguments:
        #list or dictionary of apertures
    #Returns:
        #Average counts per aperture
        #SD per aperture"""
    #counts = []
    #err = []
    #if type(aplist) == list:
        #for aper in aplist:
            #av, er = aper.stats()
            #counts.append(av)
            #err.append(er)
    #elif type(aplist) == dict:
        #for aper in aplist.itervalues():
            #av, er = aper.stats()
            #counts.append(av)
            #err.append(er)

    #counts = np.array(counts)
    #err = np.array(err)

    #return counts, err

#def getAperNumbers(fl, d):
    #"""Arguments:
        #fl = filelist, list of strings containing
                #filenames
        #d = dir, directory where fl is

    #Returns:
        #list of numbers of apertures"""


    #t = open(d + '/' + fl[0])
    #tmp = t.readlines()
    #t.close()

    #nums = []

    #for line in tmp:
        #if '#' not in line:
            #nums.append(line.split()[0])

    #return nums


#class to convert times
#class MJD(object):
    #"""
#Converts datetime object to modified Julian Date

#Methods:
    #jdn()       - return julian day number
    #jd()        - return julian day
    #mjd()       - return modified julian day
    #rjd()       - return reduced julian day
    #unix()      - return unix time

#Reqirements:
    #Must be called with datetime argument
    #"""
    #def __init__(self, datetimeobject, **kwargs):
        #object.__init__(self)


        #from datetime import datetime
        #if type(datetimeobject) != datetime:
            #print >> sys.stderr, "Argument must be datetime object"
            #sys.exit(1)

        #self._yr = datetimeobject.year
        #self._mn = datetimeobject.month
        #self._dy = datetimeobject.day

        #self._hr = datetimeobject.hour
        #self._min = datetimeobject.minute
        #self._sec = datetimeobject.second


    #def __str__(self):
        #return "%.4d-%.2d-%.2d %.2d:%.2d:%.2d" % (self._yr, self._mn, self._dy, self._hr, self._min, self._sec)


    #def jdn(self):
        #"""returns Julian day number"""
        #jdn = 367 * self._yr - (7.*(self._yr + 5001. + (self._mn - 9.)/7.))/4. + (275.*self._mn)/9. + self._dy + 1729777.
        #return jdn

    #def jd(self):
        #"""returns Julian day"""
        #jd = self.jdn() + ((self._hr - 12.) / 24.) + (self._min / 1440.) + (self._sec / 86400.)
        #return jd

    #def mjd(self):
        #"""returns modified Julian Day"""
        #return self.jd() - 2400000.5

    #def rjd(self):
        #"""returns reduced Julian Day"""
        #return self.jd() - 2400000.


    #def unix(self):
        #"""returns unix time"""
        #return (self.jd() - 2440587.5) * 86400.





#decent mkdir utility
def mkdir(newdir):
    """works the way a good mkdir should :)
        - already exists, silently complete
        - regular file in the way, raise an exception
        - parent directory(ies) does not exist, make them as well

        credit: http://code.activestate.com/recipes/82465/
    """
    if os.path.isdir(newdir):
        pass
    elif os.path.isfile(newdir):
        raise OSError("a file with the same name as the desired " \
                      "dir, '%s', already exists." % newdir)
    else:
        head, tail = os.path.split(newdir)
        if head and not os.path.isdir(head):
            mkdir(head)
        #print "_mkdir %s" % repr(newdir)
        if tail:
            os.mkdir(newdir)


def check_output(*args, **kwargs):
    '''
    Since Python 2.6 does not include this function, create it
    '''
    # Create a local copy to not overwrite the passed in argument
    kwargs = kwargs
    if 'stdout' in kwargs:
        del kwargs['stdout']
    if 'stderr' in kwargs:
        del kwargs['stderr']

    p = Popen(*args, stdout=PIPE, stderr=PIPE, **kwargs)
    result, error = p.communicate()
    if error:
        raise RuntimeError(error)

    return result
