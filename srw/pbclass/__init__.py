"""Progressbar code for python. Written in PyQt4,
the code creates a nice looking window with the
progress.

If the window does not close when you want it to,
call the pb.close() function"""

from PyQt4.QtGui import QWidget, QApplication
from interface_auto import Ui_Form
import sys

app = QApplication(sys.argv)

class progressbarClass(QWidget, Ui_Form):
    """Main progress class

    call with:
        pb = progressbarClass(N)

    where N is the total number of iterations. Then
    in whatever loop you want, call

        pb.progress(i)

    with i the index variable for the loop.

    If the window does not close when you want it to,
    call the pb.close() function"""
    def __init__(self, N, parent=None):
        """Constructor, requires a total number of
        iterations argument"""
        QWidget.__init__(self, parent)
        self.setupUi(self)

        self.progressBar.setRange(0, N)
        self.progressBar.setValue(0)

        self.counter.setText("0/%d" % N)

        self.show()

    def progress(self, val):
        """Updates the progressbar itself"""
        if val >= self.progressBar.maximum() - 1:
            self.close()
        else:
            self.progressBar.setValue(val)
            self.counter.setText("%d/%d" % (val,
                self.progressBar.maximum()))
