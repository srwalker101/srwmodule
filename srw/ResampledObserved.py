from srw import exodb
import numpy as np


def resampled_population(N, only_wasp=False):
    '''
    Use the observed population to create a synthetic population at a higher
    resolution than the observed
    '''
    if only_wasp:
        results = exodb.queryForColumns(['system.a', 'system.a_err',
            'planet.radius', 'planet.radius_err'],
            where='planet.name like "%WASP%"')
    else:
        results = exodb.queryForColumns(['system.a', 'system.a_err',
            'planet.radius', 'planet.radius_err'])

    results = [row for row in results if all(row)]

    new_a, new_rp = [], []
    for i in xrange(N):
        for planet in results:
            a = np.random.normal(*[float(val) for val in planet[:2]])
            rp = np.random.normal(*[float(val) for val in planet[2:]])

            if a > 0 and rp > 0:
                new_a.append(a)
                new_rp.append(rp)

    return new_a, new_rp



