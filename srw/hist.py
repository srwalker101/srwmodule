# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

def example_data():
    from srw import exodb
    results = exodb.queryForColumns(['planet.radius', 'planet.radius_err'],
            where='''planet.radius > 0 and planet.radius_err > 0''')
    pred = lambda row: all([type(val) != unicode for val in row])
    results = filter(pred, results)
    rp, rp_err = map(np.array, zip(*results))
    return rp, rp_err

def gaussian(data, mu, sigma):
    norm = 1. / (sigma * np.sqrt(2. * np.pi))
    arg = -0.5 * ((data - mu) / sigma) ** 2
    return norm * np.exp(arg)

def cumulative_hist(data, min_val=None, max_val=None):
    '''
    Calculates the cumulative histogram of a dataset using all of the
    data.

    - data must be a numpy array type
    - min_val and max_val allow for manual minumum and maximum values

    The norm parameter can be specified to normalise across datasets,
    or if "no" or 0 (or variants) then no normalisation will be performed
    '''
    assert isinstance(data, np.ndarray)

    s_data = np.sort(data)
    xdata, ydata = [], []

    # Shrink the array for speed, only include the values between min_val and
    # max_val
    ind = np.array([True for val in s_data])
    if min_val:
        ind &= (s_data >= min_val)
    if max_val:
        ind &= (s_data <= max_val)
    s_data = s_data[ind]


    # If min_val is below the range then add a point to the begining
    if min_val and min_val < s_data.min():
        xdata.append(min_val)
        ydata.append(0.)


    n = len(ydata)
    for val in s_data:
        xdata.append(val)
        ydata.append(float(n))
        n +=1

    if max_val and max_val > s_data.max():
        xdata.append(max_val)
        ydata.append(float(n - 1))

    xdata, ydata = [np.array(a) for a in [xdata, ydata]]

    return xdata, ydata

def weighted_hist(data, errors, x_range=None, npts=1000, normed=False):
    '''
    Combines mean values and uncertainties for the given datasets.

    Inputs:
        data    -   input values (iterable)
        errors  -   1-sigma uncertainties on data (iterable)
        x_range -   create the histogram only over this range (tuple)
        npts    -   smoothness of the plot (integer)
        normed  -   normalise the histogram (boolean)

    Returns:
        xdata   -   output x values (numpy array)
        ydata   -   output y values (numpy array)
    '''
    if not x_range:
        min_val = (data - errors).min()
        max_val = (data + errors).max()
        x_range = (min_val, max_val)

    xdata = np.linspace(x_range[0], x_range[1], npts)
    ydata = np.zeros_like(xdata)

    combined_data = zip(data, errors)

    for (y, e) in combined_data:
        ydata += gaussian(xdata, y, e)

    if normed:
        ydata /= len(combined_data)

    return xdata, ydata

def extract_histogram_data(data, bins, x_range, normed, log_data):
    _range = x_range
    if log_data:
        height, edges = np.histogram(np.log10(data), bins=bins, 
                                   range=map(np.log10, _range) if _range else None, normed=normed)
    else:
        height, edges = np.histogram(data, bins=bins, range=_range, normed=normed)

    # Pad the edges to make a nice bin shape
    edges = np.hstack([edges.min() - np.diff(edges)[0], edges])
    height = np.hstack([0, height, 0])

    return edges, height


def histogram(data, bins=10, x_range=None, normed=False, log_data=False, 
              plot_errors=False, *args, **kwargs):
    '''
    Performs what plt.hist should do with drawtype='step' but properly.

    If plot_errors == True then the errors are plotted as the poisson 
    distributed values 

    bins, range and normed are passed to np.histogram
    all other values are passed to plt.plot.
    
    If `log_data` is given then the data is binned in equal log bins
    '''
    edges, height = extract_histogram_data(data, bins, x_range, normed, log_data)
    bin_centres = edges + np.diff(edges)[0] / 2.

    try:
        ax = ax if ax else plt.gca()
    except UnboundLocalError:
        ax = plt.gca()

    if 'label' in kwargs:
        label = kwargs['label']
        del kwargs['label']
    else:
        label = None

    if plot_errors:
        errors = np.sqrt(height)

        if log_data:
            ax.errorbar(10 ** bin_centres, height, errors, ls='None', 
                        label=None,
                        *args, **kwargs)
        else:
            ax.errorbar(bin_centres, height, errors, ls='None', 
                        label=None,
                        *args, **kwargs)

    if log_data:
        plt.plot(10 ** edges, height, ls='-', marker='None', drawstyle='steps-post',
                 label=label,
                 *args, **kwargs)
    else:
        plt.plot(edges, height, ls='-', marker='None', drawstyle='steps-post',
                 label=label,
                 *args, **kwargs)

    return edges, height

def smoothed_histogram(data, x_range=None, bins=10, ofac=10., normed=False, log_data=False, width_factor=0.5,
        *args, **kwargs):
    '''
    Plot a smoothed histogram of the data.

    The arguments are the same as `histogram` apart from the `ofac` and `width_factor` arguments.

    * ofac - The oversampling from the number of true bins and the smoothed version, typically >= 10
    * width_factor - The gaussian width for the convolution, larger means more smoothing
    '''
    edges, height = extract_histogram_data(data, bins, x_range, normed, log_data)
    density = gaussian_kde(data)

    density.covariance_factor = lambda: width_factor
    density._compute_covariance()

    xs = np.linspace(edges.min(), edges.max(), ofac * edges.size)
    ys = density(xs)

    plt.plot(xs, ys, ls='-', *args, **kwargs)

    return xs, ys



if __name__ == '__main__':
    import matplotlib.pyplot as plt
    data, errors = example_data()

    xdata, ydata = weighted_histogram(data, errors, normed=True)

    plt.plot(xdata, ydata, 'r-')
    plt.show()
