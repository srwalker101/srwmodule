from setuptools import setup, Extension
import numpy
import os

numpy_incdir = numpy.get_include()
fftw_incdir = os.path.expanduser("~/build/include")

setup(name='srw',
        version='0.5.0',
        description='Mostly astronomy modules',
        author="Simon Walker",
        author_email="s.r.walker101@googlemail.com",
        packages=['srw', 'srw.NOMADParser', 'srw.AstErrors', 'srw.PyHDS'],
        package_dir={'srw': 'srw', 'srw.NOMADParser': 'srw/NOMADParser',
            'srw.AstErrors': 'srw/AstErrors',
            'srw.PyHDS': 'srw/PyHDS',
            },
        install_requires=[
            'numpy',
            'astropy',
            'matplotlib',
            'scipy',
        ]
     )
