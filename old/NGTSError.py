"""
NGTS Error module

Has two main functions, the scintillation function and the noise
lightcurve function.

SCINTILLATION FUNCTION

    scintillationFn(airmass, etime)

Inputs:
    airmass - 1d array of airmasses
    etime   - exposure time (float)

It returns a single lightcurve of the scintillation for La Palma / Paranal

NOISE FUNCTION

    noise_lightcurve(flux, skybkg, airmass, exptime, radius, extra=0.)

Inputs:
    flux, skybkg    - numpy nd arrays
    airmass         - numpy 1d array
    radius          - float of the aperture radius
    extra           - any extra component
"""

import numpy as np
import srw

def scintillationFn(airmass, etime):
    """Scintillation function, works on arrays or single
    values"""
    height = 2396.
    return 0.004*0.18**(-2./3.)*airmass**(7./4.)*np.exp(-height/8000)*\
            (2.*etime)**(-0.5)

def noise_lightcurve(flux, skybkg, airmass, exptime, radius, rdnoise, extra=0.):
    """
    Function requires data (in Sysrem-compatible form):
        The 2d flux array of apertures and frame number
        The 2d sky background array
        A 1d time series of the airmass
        An exposure time for the data set (s)
        A read noise parameter (e- per pix)
        And an (optional) extra unknown component.

    This function returns a dictionary of results with the combined total error value
    and the individual components (not fractional):

        keyname        returned
        -------        --------
        combined       the errors in quadrature
        scintillation  the scintillation component
        skybkg         the sky background component
        source         the component from the source
        readnoise      the readnoise component
    """
    scintillation = scintillationFn(airmass, exptime)

    mags = srw.ZP(exptime) - np.log10(flux)


    avflux = np.average(flux, axis=1)
    stdflux = np.std(flux, axis=1)
    avmags = np.average(mags, axis=1)

    area = radius**2 * np.pi
    rdnoise_contrib = np.sqrt(rdnoise**2 * area)

    scintillation_contrib = scintillation * np.ones(flux.shape) * flux
    sky_contrib = np.sqrt(skybkg * area)
    source_contrib = np.sqrt(flux)

    averr = np.average(np.sqrt(scintillation_contrib**2 + sky_contrib**2 + source_contrib**2 + extra**2 + rdnoise_contrib**2), axis=1)

    errsdict = {}

    errsdict['combined'] = averr
    errsdict['scintillation'] = np.average(scintillation_contrib, axis=1)
    errsdict['skybkg'] = np.average(sky_contrib, axis=1)
    errsdict['source'] = np.average(source_contrib, axis=1)
    errsdict['readnoise'] = rdnoise_contrib * np.ones(flux.shape[0])

    return errsdict
