# -*- coding: utf-8 -*-
import srw.stats as s
import pyfits
import os

def check_close(a, b, toler=0.01):
    return abs(((a - b) / b)) < toler

def test_mad():
    data = [1, 2, 3, 5, 2, 1, 2, 5, 2, 1, 2, 5, 6, 7, 3, 1, 2, 32, 3]
    first_mad = s.mad(data)
    assert first_mad > 0

    median = s.np.median(data)
    second_mad = s.mad(data, median=median)
    assert check_close(first_mad, second_mad)

def test_2d_mad():
    flux = pyfits.getdata(os.path.join(os.path.dirname(__file__),
                                       'rms_data.fits'))
    mad = s.mad(flux, axis=0)
    assert len(mad.shape) == 1
    assert mad.size == flux.shape[1]
    assert (mad > 0).all()

    mad = s.mad(flux, axis=1)
    assert len(mad.shape) == 1
    assert mad.size == flux.shape[0]
    assert (mad > 0).all()


