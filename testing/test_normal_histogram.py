import matplotlib
matplotlib.use('Agg')
from srw.hist import histogram
import numpy as np
import os
import matplotlib.pyplot as plt

def test_histogram():
    data = np.random.normal(0., 1., 100)
    histogram(data)
    plt.savefig(os.path.join(os.path.dirname(__file__), 'hist_test.png'))
    plt.close()

def test_log_values():
    data = 10 ** np.random.normal(0., 1., 100)

    histogram(data, log_data=True)
    plt.xscale('log')
    plt.savefig(os.path.join(os.path.dirname(__file__), 'hist_test_log.png'))
    plt.close()

def test_correct_labellling():
    '''
    Ensure that the errorbars (if required) are not labelled
    '''
    data = 10 ** np.random.normal(0., 1., 100)
    histogram(data, plot_errors=True, label="Test label")
    plt.legend(loc='best')
    plt.savefig(os.path.join(os.path.dirname(__file__), 
                             'hist_test_labels.png'))
    plt.close()
