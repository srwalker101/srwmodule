from srw.hist import smoothed_histogram
import numpy as np
import matplotlib.pyplot as plt
import os

def test_smoothed_histogram():
    data = np.hstack([np.random.normal(-10., 5., 100),
        np.random.normal(15, 3., 200)])
    width_factor = 0.3

    smoothed_histogram(data, width_factor=width_factor)

    outfile_name = os.path.join(os.path.dirname(__file__) or '.',
        'smoothed-histogram.png')
    plt.savefig(outfile_name)

    assert os.path.isfile(outfile_name)


