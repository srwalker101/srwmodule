from srw.hist import cumulative_hist
import numpy as np

def test_stuff():
    xdata = np.random.random(100)

    xdata, ydata = cumulative_hist(xdata)

    assert (np.diff(xdata) >= 0).all()
    assert (np.diff(ydata) >= 0).all()


