# -*- coding: utf-8 -*-

import pyfits
import srw.stats as s
import os

def setup_module(module):
    with pyfits.open(os.path.join(os.path.dirname(__file__),
                                  'rms_data.fits')) as infile:
        module.flux = infile['flux'].data
        module.fluxerr = infile['fluxerr'].data

def test_rms_vs_brightness():
    av_flux, frms = s.rms_vs_brightness(flux)
    assert av_flux.size == flux.shape[0]
    assert frms.size == flux.shape[0]
    assert (av_flux > 0).all()

def test_median_version():
    av_flux, frms = s.rms_vs_brightness(flux, median_method=True)
    assert av_flux.size == flux.shape[0]
    assert frms.size == flux.shape[0]
    assert (av_flux > 0).all()

def test_fluxerr_version():
    av_flux, frms = s.rms_vs_brightness(flux, fluxerr)
    assert av_flux.size == flux.shape[0]
    assert frms.size == flux.shape[0]
    assert (av_flux > 0).all()
